<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', function () {
    return view('index');
});

// ===============< Frontend Part Starts >============

//Homepage
Route::get('/', [
	'uses' => 'Frontend\HomepageController@index',
	'as' => 'index'
]);
Route::get('/homepage/create', [
	'uses' => 'Frontend\HomepageController@create',
	'as' => 'frontend.homepage.create'
]);
Route::post('/homepage/store', [
	'uses' => 'Frontend\HomepageController@store',
	'as' => 'frontend.homepage.store'
]);
Route::get('/homepage/show/{id}', [
	'uses' => 'Frontend\HomepageController@show',
	'as' => 'frontend.homepage.show'
]);
Route::post('/homepage/update/{id}', [
	'uses' => 'Frontend\HomepageController@update',
	'as' => 'frontend.homepage.update'
]);
Route::get('/homepage/destroy/{id}',[
	'uses'=>'Frontend\homepageController@destroy',
	'as'=>'frontend.homepage.destroy'
]);

//Career
Route::get('/career', [
	'uses' => 'Frontend\CareerController@index',
	'as' => 'frontend.career'
]);
Route::get('/career/create', [
	'uses' => 'Frontend\CareerController@create',
	'as' => 'frontend.career.create'
]);
Route::post('/career/store', [
	'uses' => 'Frontend\CareerController@store',
	'as' => 'frontend.career.store'
]);
Route::get('/career/show/{id}', [
	'uses' => 'Frontend\CareerController@show',
	'as' => 'frontend.career.show'
]);
Route::post('/career/update/{id}', [
	'uses' => 'Frontend\CareerController@update',
	'as' => 'frontend.career.update'
]);
Route::get('/career/destroy/{id}',[
	'uses'=>'Frontend\CareerController@destroy',
	'as'=>'frontend.career.destroy'
]);

// ================< Frontend Part Ends >=============



Auth::routes();

// Admin Dashboard
Route::get('/admin', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');


// ===============< Website Part Starts >============

// Home Page
Route::get('/admin/website/homepage', [
	'uses' => 'Website\HomepageController@index',
	'as' => 'homepage'
]);
Route::get('/admin/website/homepage/create', [
	'uses' => 'Website\HomepageController@create',
	'as' => 'homepage.create'
]);
Route::post('/admin/website/homepage/store', [
	'uses' => 'Website\HomepageController@store',
	'as' => 'homepage.store'
]);
Route::get('/admin/website/homepage/show/{id}', [
	'uses' => 'Website\HomepageController@show',
	'as' => 'homepage.show'
]);
Route::post('/admin/website/homepage/update/{id}', [
	'uses' => 'Website\HomepageController@update',
	'as' => 'homepage.update'
]);
Route::get('/admin/website/homepage/destroy/{id}',[
	'uses'=>'Website\HomepageController@destroy',
	'as'=>'homepage.destroy'
]);

//About
Route::get('/admin/website/about', [
	'uses' => 'Website\AboutController@index',
	'as' => 'about'
]);
Route::get('/admin/website/about/create', [
	'uses' => 'Website\AboutController@create',
	'as' => 'about.create'
]);
Route::post('/admin/website/about/store', [
	'uses' => 'Website\AboutController@store',
	'as' => 'about.store'
]);
Route::get('/admin/website/about/show/{id}', [
	'uses' => 'Website\AboutController@show',
	'as' => 'about.show'
]);
Route::post('/admin/website/about/update/{id}', [
	'uses' => 'Website\AboutController@update',
	'as' => 'about.update'
]);
Route::get('/admin/website/about/destroy/{id}',[
	'uses'=>'Website\AboutController@destroy',
	'as'=>'about.destroy'
]);

//Service
Route::get('/admin/website/service', [
	'uses' => 'Website\ServiceController@index',
	'as' => 'service'
]);
Route::get('/admin/website/service/create', [
	'uses' => 'Website\ServiceController@create',
	'as' => 'service.create'
]);
Route::post('/admin/website/service/store', [
	'uses' => 'Website\ServiceController@store',
	'as' => 'service.store'
]);
Route::get('/admin/website/service/show/{id}', [
	'uses' => 'Website\ServiceController@show',
	'as' => 'service.show'
]);
Route::post('/admin/website/service/update/{id}', [
	'uses' => 'Website\ServiceController@update',
	'as' => 'service.update'
]);
Route::get('/admin/website/service/destroy/{id}',[
	'uses'=>'Website\ServiceController@destroy',
	'as'=>'service.destroy'
]);

//Team
Route::get('/admin/website/team', [
	'uses' => 'Website\TeamController@index',
	'as' => 'team'
]);
Route::get('/admin/website/team/create', [
	'uses' => 'Website\TeamController@create',
	'as' => 'team.create'
]);
Route::post('/admin/website/team/store', [
	'uses' => 'Website\TeamController@store',
	'as' => 'team.store'
]);
Route::get('/admin/website/team/show/{id}', [
	'uses' => 'Website\TeamController@show',
	'as' => 'team.show'
]);
Route::post('/admin/website/team/update/{id}', [
	'uses' => 'Website\TeamController@update',
	'as' => 'team.update'
]);
Route::get('/admin/website/team/destroy/{id}',[
	'uses'=>'Website\TeamController@destroy',
	'as'=>'team.destroy'
]);

//Workflow
// Route::get('/admin/website/workflow', [
// 	'uses' => 'Website\WorkflowController@index',
// 	'as' => 'workflow'
// ]);
// Route::get('/admin/website/workflow/create', [
// 	'uses' => 'Website\WorkflowController@create',
// 	'as' => 'workflow.create'
// ]);
// Route::post('/admin/website/workflow/store', [
// 	'uses' => 'Website\WorkflowController@store',
// 	'as' => 'workflow.store'
// ]);
// Route::get('/admin/website/workflow/show/{id}', [
// 	'uses' => 'Website\WorkflowController@show',
// 	'as' => 'workflow.show'
// ]);
// Route::post('/admin/website/workflow/update/{id}', [
// 	'uses' => 'Website\WorkflowController@update',
// 	'as' => 'workflow.update'
// ]);
// Route::get('/admin/website/workflow/destroy/{id}',[
// 	'uses'=>'Website\WorkflowController@destroy',
// 	'as'=>'workflow.destroy'
// ]);

//Contact
Route::get('/admin/website/contact', [
	'uses' => 'Website\ContactController@index',
	'as' => 'contact'
]);
Route::get('/admin/website/contact/create', [
	'uses' => 'Website\ContactController@create',
	'as' => 'contact.create'
]);
Route::post('/admin/website/contact/store', [
	'uses' => 'Website\ContactController@store',
	'as' => 'contact.store'
]);
Route::get('/admin/website/contact/show/{id}', [
	'uses' => 'Website\ContactController@show',
	'as' => 'contact.show'
]);
Route::post('/admin/website/contact/update/{id}', [
	'uses' => 'Website\ContactController@update',
	'as' => 'contact.update'
]);
Route::get('/admin/website/contact/destroy/{id}',[
	'uses'=>'Website\ContactController@destroy',
	'as'=>'contact.destroy'
]);

//Footer
Route::get('/admin/website/footer', [
	'uses' => 'Website\FooterController@index',
	'as' => 'footer'
]);
Route::get('/admin/website/footer/create', [
	'uses' => 'Website\FooterController@create',
	'as' => 'footer.create'
]);
Route::post('/admin/website/footer/store', [
	'uses' => 'Website\FooterController@store',
	'as' => 'footer.store'
]);
Route::get('/admin/website/footer/show/{id}', [
	'uses' => 'Website\FooterController@show',
	'as' => 'footer.show'
]);
Route::post('/admin/website/footer/update/{id}', [
	'uses' => 'Website\FooterController@update',
	'as' => 'footer.update'
]);
Route::get('/admin/website/footer/destroy/{id}',[
	'uses'=>'Website\FooterController@destroy',
	'as'=>'footer.destroy'
]);


// ================< Website Part Ends >=============







// ============< Extra Pages Starts >===============

// Blog
Route::get('/admin/extra/blog', [
	'uses' => 'Extra\BlogController@index',
	'as' => 'blog'
]);
Route::get('/admin/extra/blog/create', [
	'uses' => 'Extra\BlogController@create',
	'as' => 'blog.create'
]);
Route::post('/admin/extra/blog/store', [
	'uses' => 'Extra\BlogController@store',
	'as' => 'blog.store'
]);
Route::get('/admin/extra/blog/show/{id}', [
	'uses' => 'Extra\BlogController@show',
	'as' => 'blog.show'
]);
Route::post('/admin/extra/blog/update/{id}', [
	'uses' => 'Extra\BlogController@update',
	'as' => 'blog.update'
]);
Route::get('/admin/extra/blog/destroy/{id}',[
	'uses'=>'Extra\BlogController@destroy',
	'as'=>'blog.destroy'
]);

// Help Center
Route::get('/admin/extra/help', [
	'uses' => 'Extra\HelpController@index',
	'as' => 'help'
]);
Route::get('/admin/extra/help/create', [
	'uses' => 'Extra\HelpController@create',
	'as' => 'help.create'
]);
Route::post('/admin/extra/help/store', [
	'uses' => 'Extra\HelpController@store',
	'as' => 'help.store'
]);
Route::get('/admin/extra/help/show/{id}', [
	'uses' => 'Extra\HelpController@show',
	'as' => 'help.show'
]);
Route::post('/admin/extra/help/update/{id}', [
	'uses' => 'Extra\HelpController@update',
	'as' => 'help.update'
]);
Route::get('/admin/extra/help/destroy/{id}',[
	'uses'=>'Extra\HelpController@destroy',
	'as'=>'help.destroy'
]);

// Community
Route::get('/admin/extra/community', [
	'uses' => 'Extra\CommunityController@index',
	'as' => 'community'
]);
Route::get('/admin/extra/community/create', [
	'uses' => 'Extra\CommunityController@create',
	'as' => 'community.create'
]);
Route::post('/admin/extra/community/store', [
	'uses' => 'Extra\CommunityController@store',
	'as' => 'community.store'
]);
Route::get('/admin/extra/community/show/{id}', [
	'uses' => 'Extra\CommunityController@show',
	'as' => 'community.show'
]);
Route::post('/admin/extra/community/update/{id}', [
	'uses' => 'Extra\CommunityController@update',
	'as' => 'community.update'
]);
Route::get('/admin/extra/community/destroy/{id}',[
	'uses'=>'Extra\CommunityController@destroy',
	'as'=>'community.destroy'
]);

// Marketplace
Route::get('/admin/extra/marketplace', [
	'uses' => 'Extra\MarketplaceController@index',
	'as' => 'marketplace'
]);
Route::get('/admin/extra/marketplace/create', [
	'uses' => 'Extra\MarketplaceController@create',
	'as' => 'marketplace.create'
]);
Route::post('/admin/extra/marketplace/store', [
	'uses' => 'Extra\MarketplaceController@store',
	'as' => 'marketplace.store'
]);
Route::get('/admin/extra/marketplace/show/{id}', [
	'uses' => 'Extra\MarketplaceController@show',
	'as' => 'marketplace.show'
]);
Route::post('/admin/extra/marketplace/update/{id}', [
	'uses' => 'Extra\MarketplaceController@update',
	'as' => 'marketplace.update'
]);
Route::get('/admin/extra/marketplace/destroy/{id}',[
	'uses'=>'Extra\MarketplaceController@destroy',
	'as'=>'marketplace.destroy'
]);

// Career
Route::get('/admin/extra/career', [
	'uses' => 'Extra\CareerController@index',
	'as' => 'career'
]);
Route::get('/admin/extra/career/create', [
	'uses' => 'Extra\CareerController@create',
	'as' => 'career.create'
]);
Route::post('/admin/extra/career/store', [
	'uses' => 'Extra\CareerController@store',
	'as' => 'career.store'
]);
Route::get('/admin/extra/career/show/{id}', [
	'uses' => 'Extra\CareerController@show',
	'as' => 'career.show'
]);
Route::get('/admin/extra/career/show/edit/{id}', [
	'uses' => 'Extra\CareerController@edit',
	'as' => 'career.edit'
]);
Route::post('/admin/extra/career/update/{id}', [
	'uses' => 'Extra\CareerController@update',
	'as' => 'career.update'
]);
Route::get('/admin/extra/career/destroy/{id}',[
	'uses'=>'Extra\CareerController@destroy',
	'as'=>'career.destroy'
]);

// =============< Extra Pages Ends >================
