<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Admin\Website\ServiceSection; //ServiceSection Model
use Illuminate\Support\Facades\Storage; //Storing Images

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.website.service.index')->with('servicesections', ServiceSection::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.website.service.create')->with('servicesections', ServiceSection::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_one_image'=>'required',
            'service_one_title'=>'required',
            'service_one_details'=>'required',

            'service_two_image'=>'required',
            'service_two_title'=>'required',
            'service_two_details'=>'required',

            'service_three_image'=>'required',
            'service_three_title'=>'required',
            'service_three_details'=>'required',

            'service_four_image'=>'required',
            'service_four_title'=>'required',
            'service_four_details'=>'required',

            'service_five_image'=>'required',
            'service_five_title'=>'required',
            'service_five_details'=>'required',

            'service_six_image'=>'required',
            'service_six_title'=>'required',
            'service_six_details'=>'required',
        ]);

        $service = new ServiceSection(); //For Allocating a New Row in Database

        if($request->hasFile('service_one_image')){
            $path = time().'service_one.'.$request->service_one_image->getClientOriginalExtension();
            $request->service_one_image->move(public_path('images'), $path);
            $service->service_one_image = $path;
        } //service_one_image
        $service->service_one_title = $request->service_one_title; //service_one_title
        $service->service_one_details = $request->service_one_details; //service_one_details



        if($request->hasFile('service_two_image')){
            $path = time().'service_two.'.$request->service_two_image->getClientOriginalExtension();
            $request->service_two_image->move(public_path('images'), $path);
            $service->service_two_image = $path;
        } //service_two_image
        $service->service_two_title = $request->service_two_title; //service_two_title
        $service->service_two_details = $request->service_two_details; //service_two_details



        if($request->hasFile('service_three_image')){
            $path = time().'service_three.'.$request->service_three_image->getClientOriginalExtension();
            $request->service_three_image->move(public_path('images'), $path);
            $service->service_three_image = $path;
        } //service_three_image
        $service->service_three_title = $request->service_three_title; //service_three_title
        $service->service_three_details = $request->service_three_details; //service_three_details



        if($request->hasFile('service_four_image')){
            $path = time().'service_four.'.$request->service_four_image->getClientOriginalExtension();
            $request->service_four_image->move(public_path('images'), $path);
            $service->service_four_image = $path;
        } //service_four_image
        $service->service_four_title = $request->service_four_title; //service_four_title
        $service->service_four_details = $request->service_four_details; //service_four_details



        if($request->hasFile('service_five_image')){
            $path = time().'service_five.'.$request->service_five_image->getClientOriginalExtension();
            $request->service_five_image->move(public_path('images'), $path);
            $service->service_five_image = $path;
        } //service_five_image
        $service->service_five_title = $request->service_five_title; //service_five_title
        $service->service_five_details = $request->service_five_details; //service_five_details



        if($request->hasFile('service_six_image')){
            $path = time().'service_six.'.$request->service_six_image->getClientOriginalExtension();
            $request->service_six_image->move(public_path('images'), $path);
            $service->service_six_image = $path;
        } //service_six_image
        $service->service_six_title = $request->service_six_title; //service_six_title
        $service->service_six_details = $request->service_six_details; //service_six_details



        $service->save(); //For Storing the inputed data in database
        return redirect()->route('service'); //After submit it will redirect to index page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.website.service.show')->with('servicesection', ServiceSection::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $findService = ServiceSection::find($id); //For Allocating a New Row in Database

        if($request->hasFile('service_one_image')){
            $path = time().'service_one.'.$request->service_one_image->getClientOriginalExtension();
            $request->service_one_image->move(public_path('images'), $path);
            $findService->service_one_image = $path;
        } //service_one_image
        $findService->service_one_title = $request->service_one_title; //service_one_title
        $findService->service_one_details = $request->service_one_details; //service_one_details



        if($request->hasFile('service_two_image')){
            $path = time().'service_two.'.$request->service_two_image->getClientOriginalExtension();
            $request->service_two_image->move(public_path('images'), $path);
            $findService->service_two_image = $path;
        } //service_two_image
        $findService->service_two_title = $request->service_two_title; //service_two_title
        $findService->service_two_details = $request->service_two_details; //service_two_details



        if($request->hasFile('service_three_image')){
            $path = time().'service_three.'.$request->service_three_image->getClientOriginalExtension();
            $request->service_three_image->move(public_path('images'), $path);
            $findService->service_three_image = $path;
        } //service_three_image
        $findService->service_three_title = $request->service_three_title; //service_three_title
        $findService->service_three_details = $request->service_three_details; //service_three_details



        if($request->hasFile('service_four_image')){
            $path = time().'service_four.'.$request->service_four_image->getClientOriginalExtension();
            $request->service_four_image->move(public_path('images'), $path);
            $findService->service_four_image = $path;
        } //service_four_image
        $findService->service_four_title = $request->service_four_title; //service_four_title
        $findService->service_four_details = $request->service_four_details; //service_four_details



        if($request->hasFile('service_five_image')){
            $path = time().'service_five.'.$request->service_five_image->getClientOriginalExtension();
            $request->service_five_image->move(public_path('images'), $path);
            $findService->service_five_image = $path;
        } //service_five_image
        $findService->service_five_title = $request->service_five_title; //service_five_title
        $findService->service_five_details = $request->service_five_details; //service_five_details



        if($request->hasFile('service_six_image')){
            $path = time().'service_six.'.$request->service_six_image->getClientOriginalExtension();
            $request->service_six_image->move(public_path('images'), $path);
            $findService->service_six_image = $path;
        } //service_six_image
        $findService->service_six_title = $request->service_six_title; //service_six_title
        $findService->service_six_details = $request->service_six_details; //service_six_details



        $findService->save(); //For Storing the inputed data in database
        return redirect()->route('service'); //After submit it will redirect to index page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
