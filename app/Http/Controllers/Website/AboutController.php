<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Admin\Website\AboutSection; //AboutSection Model
use Illuminate\Support\Facades\Storage; //Storing Images

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.website.about.index')->with('aboutsections', AboutSection::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.website.about.create')->with('aboutsections', AboutSection::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'about_image'=>'required',
            'about_heading'=>'required',
            'about_details'=>'required',
        ]);

        $about = new AboutSection(); //For Allocating a New Row in Database

        if($request->hasFile('about_image')){
            $path = time().'about.'.$request->about_image->getClientOriginalExtension();
            $request->about_image->move(public_path('images'), $path);
            $about->about_image = $path;
        } //For Storing the about image

        $about->about_heading = $request->about_heading; //For Storing the about heading
        $about->about_details = $request->about_details; //For Storing the about details
        $about->linkdin = $request->linkdin; //For Storing the linkdin
        $about->skype = $request->skype; //For Storing the skype
        $about->facebook = $request->facebook; //For Storing the facebook
        $about->twitter = $request->twitter; //For Storing the twitter
        $about->whatsapp = $request->whatsapp; //For Storing the whatsapp
        $about->viber = $request->viber; //For Storing the viber
        $about->telegram = $request->telegram; //For Storing the telegram
        $about->google_plus = $request->google_plus; //For Storing the google_plus

        $about->save(); //For Storing the inputed data in database
        
        return redirect()->route('about'); //After submit it will redirect to index page

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.website.about.show')->with('aboutsection', AboutSection::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $findAbout = AboutSection::find($id);

        if($request->hasFile('about_image')){
            $path = time().'new_about.'.$request->about_image->getClientOriginalExtension();
            $request->about_image->move(public_path('images'), $path);
            $findAbout->about_image = $path;
        }

        $findAbout->about_heading = $request->about_heading;
        $findAbout->about_details = $request->about_details; //For Storing the about details
        $findAbout->linkdin = $request->linkdin; //For Storing the linkdin
        $findAbout->skype = $request->skype; //For Storing the skype
        $findAbout->facebook = $request->facebook; //For Storing the facebook
        $findAbout->twitter = $request->twitter; //For Storing the twitter
        $findAbout->whatsapp = $request->whatsapp; //For Storing the whatsapp
        $findAbout->viber = $request->viber; //For Storing the viber
        $findAbout->telegram = $request->telegram; //For Storing the telegram
        $findAbout->google_plus = $request->google_plus; //For Storing the google_plus

        $findAbout->save();
        
        return redirect()->route('about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
