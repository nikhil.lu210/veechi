<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Admin\Website\HomeSection; //HomeSection Model
use Illuminate\Support\Facades\Storage; //Storing Images

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.website.home.index')->with('homesections', HomeSection::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.website.home.create')->with('homesections', HomeSection::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'company_name'=>'required',
            'short_note'=>'required'
        ]);
        
        $home = new HomeSection(); //For Allocating a New Row in Database

        if($request->hasFile('company_logo')){
            $path = time().'.'.$request->company_logo->getClientOriginalExtension();
            $request->company_logo->move(public_path('images'), $path);
            $home->company_logo = $path;
        } //For Storing the company logo

        $home->company_moto = $request->company_moto; //For Storing the company moto
        $home->company_name = $request->company_name; //For Storing the company name
        $home->short_note = $request->short_note; //For Storing the company short note

        if($request->hasFile('background_image')){
            $path = time().'123.'.$request->background_image->getClientOriginalExtension();
            $request->background_image->move(public_path('images'), $path);
            $home->background_image = $path;
        } //For Storing the background image to the right container

        $home->save(); //For Storing the inputed data in database
        
        return redirect()->route('homepage'); //After submit it will redirect to index page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.website.home.show')->with('homesection', HomeSection::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name'=>'required',
            'short_note'=>'required'
        ]);

        $findHome = HomeSection::find($id);

        if($request->hasFile('company_logo')){
            $path = time().'a1.'.$request->company_logo->getClientOriginalExtension();
            $request->company_logo->move(public_path('images'), $path);
            $findHome->company_logo = $path;
        }

        $findHome->company_moto = $request->company_moto;
        $findHome->company_name = $request->company_name;
        $findHome->short_note = $request->short_note;

        if($request->hasFile('background_image')){
            $path = time().'a2.'.$request->background_image->getClientOriginalExtension();
            $request->background_image->move(public_path('images'), $path);
            $findHome->background_image = $path;
        }

        $findHome->save();
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
