<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Admin\Website\TeamSection; //TeamSection Model
use Illuminate\Support\Facades\Storage; //Storing Images

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.website.team.index')->with('teamsections', TeamSection::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.website.team.create')->with('teamsections', TeamSection::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'designation'=>'required',
            'facebook'=>'required',
            'linkdin'=>'required',
            'twitter'=>'required',
            'skype'=>'required',
        ]);

        $team = new TeamSection(); //For Allocating a New Row in Database

        if ($request -> hasFile('profile_picture')) {
            $path = time().'team.'.$request->profile_picture->getClientOriginalExtension();
        
            $resize_image = Image::make($request->file('profile_picture'))->resize(400, 400)->encode('jpg');
            // $resize_image->save(public_path('/images'), $path);
            $resize_image->save('images/' . $path, 100);
            // dd($path);
        
            $team->profile_picture = $path;
        } //For Storing the team image

        $team->name = $request->name; //For Storing the name
        $team->designation = $request->designation; //For Storing the designation
        $team->facebook = $request->facebook; //For Storing the facebook
        $team->linkdin = $request->linkdin; //For Storing the linkdin
        $team->twitter = $request->twitter; //For Storing the twitter
        $team->skype = $request->skype; //For Storing the skype

        $team->save(); //For Storing the inputed data in database
        
        return redirect()->route('team'); //After submit it will redirect to index page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.website.team.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
