<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class HomeSection extends Model
{
    protected $fillable = [
        'company_logo',
        'company_moto',
        'company_name',
        'short_note',
        'background_image'
    ];
}
