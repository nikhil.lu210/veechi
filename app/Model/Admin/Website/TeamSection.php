<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class TeamSection extends Model
{
    protected $fillable = [
        'profile_picture',
        'name',
        'designation',
        'facebook',
        'facebook',
        'linkdin',
        'twitter',
        'skype',
    ];
}
