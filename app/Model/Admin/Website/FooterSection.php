<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class FooterSection extends Model
{
    protected $fillable = [
        '',
    ];
}
