<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class ServiceSection extends Model
{
    protected $fillable = [
        'service_one_image',
        'service_one_title',
        'service_one_details',

        'service_two_image',
        'service_two_title',
        'service_two_details',
        
        'service_three_image',
        'service_three_title',
        'service_three_details',
        
        'service_four_image',
        'service_four_title',
        'service_four_details',
        
        'service_five_image',
        'service_five_title',
        'service_five_details',
        
        'service_six_image',
        'service_six_title',
        'service_six_details'
    ];
}
