<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class AboutSection extends Model
{
    protected $fillable = [
        'about_image',
        'about_heading',
        'company_details',
        'linkdin',
        'skype',
        'facebook',
        'twitter',
        'whatsapp',
        'viber',
        'telegram',
        'google_plus'
    ];
}
