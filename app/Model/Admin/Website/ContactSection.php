<?php

namespace App\Model\Admin\Website;

use Illuminate\Database\Eloquent\Model;

class ContactSection extends Model
{
    protected $fillable = [
        'subject',
        'name',
        'email',
        'message'
    ];
}
