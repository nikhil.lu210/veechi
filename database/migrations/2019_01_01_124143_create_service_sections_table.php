<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('service_one_image');
            $table->string('service_one_title');
            $table->text('service_one_details');
            
            $table->binary('service_two_image');
            $table->string('service_two_title');
            $table->text('service_two_details');

            $table->binary('service_three_image');
            $table->string('service_three_title');
            $table->text('service_three_details');

            $table->binary('service_four_image');
            $table->string('service_four_title');
            $table->text('service_four_details');

            $table->binary('service_five_image');
            $table->string('service_five_title');
            $table->text('service_five_details');

            $table->binary('service_six_image');
            $table->string('service_six_title');
            $table->text('service_six_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_sections');
    }
}
