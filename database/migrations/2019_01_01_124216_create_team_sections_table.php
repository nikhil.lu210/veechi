<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('profile_picture')->nullable();
            $table->string('name');
            $table->string('designation');
            $table->string('facebook');
            $table->string('linkdin');
            $table->string('twitter');
            $table->string('skype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_sections');
    }
}
