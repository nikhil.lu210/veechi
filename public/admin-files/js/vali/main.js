// (function () {
// 	"use strict";

// 	var treeviewMenu = $('.app-menu');

// 	// Toggle Sidebar
// 	$('[data-toggle="sidebar"]').click(function(event) {
// 		event.preventDefault();
// 		$('.app').toggleClass('sidenav-toggled');
// 	});

// 	// Activate sidebar treeview toggle
// 	$("[data-toggle='treeview']").click(function(event) {
// 		event.preventDefault();
// 		if(!$(this).parent().hasClass('is-expanded')) {
// 			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
// 		}
// 		$(this).parent().toggleClass('is-expanded');
// 	});

// 	// Set initial active toggle
// 	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

// 	//Activate bootstrip tooltips
// 	$("[data-toggle='tooltip']").tooltip();

// })();


// $(function () {
// 	/*$('#app-menu a').click(function(){
// 		$(window).load(function(){
// 			$(this).parent().addClass('active');
// 		});
// 	});*/
// 	var url = window.location.href;
//     var activePage = url;
//     $('.app-menu a').each(function () {
//         var linkPage = this.href;

//         if (activePage == linkPage) {
//             $(this).addClass("active");
//         }
//     });
// });


(function () {
    // "use strict";

    var treeviewMenu = $('.app-menu');

    // Toggle Sidebar
    $('[data-toggle="sidebar"]').click(function(event) {
        event.preventDefault();
        $('.app').toggleClass('sidenav-toggled');
    });

    // Activate sidebar treeview toggle
    $("[data-toggle='treeview']").click(function(event) {
        event.preventDefault();
        if(!$(this).parent().hasClass('is-expanded')) {
            treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
        }
        $(this).parent().toggleClass('is-expanded');
    });

    // Set initial active toggle
    $("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

    //Activate bootstrip tooltips
    // $("[data-toggle='tooltip']").tooltip();

})();


$(function(){
    var url = window.location.href;
    var activePage = url;
    $('.app-menu a').each(function () {
        var linkPage = this.href;

        if (activePage == linkPage) {
            $(this).addClass("active");
            $(this).parent().parent().closest("ul").parent().addClass('is-expanded');
        }
        else{
            $('.treeview .app-menu__item').removeClass("active");
        }
    });
});