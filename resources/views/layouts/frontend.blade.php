<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.frontend.header') 
    {{--  This will include all CSS files which are connected into header.blade.php in partials Folder  --}}
</head>
<body class="app sidebar-mini rtl">
    <div id="app">

        <main class="app-content">
            @yield('content')
        </main>
        
    </div>

    @include('partials.frontend.javascript')
    {{--  This will include all JS files which are connected into javascript.blade.php in partials Folder  --}}
    

</body>
</html>