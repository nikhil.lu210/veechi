<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    {{-- <!-- CSRF Token -->--}}
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    
    {{-- Page Title --}}
    <title> Veechi Technology </title>

    {{-- Icon in Tab --}}
    <link rel="shortcut icon" href="{{ asset('files/images/logo01.png')}}"> 
    
    {{-- Google-Fonts CSS --}}
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    
    {{-- Font-Awesome CSS --}}
    <link rel="stylesheet" href="{{ asset('files/css/font-awesome/all.min.css') }}"> 
    
    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="{{ asset('files/css/bootstrap/bootstrap.min.css') }}"> 
    
    {{-- Carousel CSS --}}
    {{--  <link rel="stylesheet" href="{{ asset('files/css/carousel/owl.carousel.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('files/css/carousel/carousel.css') }}"> 
    <link rel="stylesheet" href="{{ asset('files/css/carousel/global-style.css') }}">   --}}
    <link rel="stylesheet" href="{{ asset('files/css/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('files/css/owl-carousel/owl.theme.default.css') }}">
    
    {{-- Admin Custom CSS --}}
    <link rel="stylesheet" href="{{ asset('files/css/margin-padding.css')}}">
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('files/css/responsive.css')}}">
</head>

<body>
    {{-- ================= < Navbar Part Starts>================= --}}
    <nav class="navbar navbar-expand-lg navbar-light bg-none">
        {{-- <a class="navbar-brand" href="#">Navbar</a> --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    
        <div class="container">
            <div class="navbar-collapse collapse w-100 justify-content-center" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#service">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#workflow">Workflow</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    {{-- ================== < Navbar Part Ends>================== --}}



    {{-- ================= < Carousel Part Starts>================= --}}
    @foreach ($homesections as $homesection)
    <section class="carousel-part p-b-40" id="home">
        <div class="container">
            <div class="row">
                <div class="col-md-6 large-device">
                    <div class="veechi-logo text-center m-t-60">
                        @if (isset($homesection->company_logo))
                            <figure><img src="{{ asset('images/'.$homesection->company_logo) }}" alt="L O G O" width="20%"></figure>
                        @else
                            <figure><img src="{{ asset('files/images/logo02.png') }}" alt="L O G O" width="20%"></figure>                            
                        @endif
                    </div>
                    <div class="page-caption">
                        {{--  Company Moto  --}}
                        <h6 class="veechi-moto text-center">
                            @if (isset($homesection->company_moto))
                                {{$homesection->company_moto}}
                            @else
                                We solve UX problems to achive your business goal...!
                            @endif
                        </h6>

                        {{--  Company Name  --}}
                        <h1 class="veechi-welcome m-t-60">
                            welcome to <br>
                            @if (isset($homesection->company_name))
                                <span class="veechi">{{$homesection->company_name}}</span>
                            @else
                                <span class="veechi">Veechi <span class="tech">Technologies</span></span>
                            @endif
                        </h1>

                        {{--  Short Note  --}}
                        <p class="veechi-info">
                            @if (isset($homesection->short_note))
                                {{$homesection->short_note}}
                            @else
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia aliquid labore architecto, quas quod quidem voluptatem perferendis blanditiis excepturi adipisci!
                            @endif
                        </p>
                        <div class="row p-t-50">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-6">
                                        <a href="" class="btn btn-success btn-lg btn-block btn-custom">Get Started</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="" class="btn btn-outline-success btn-lg btn-block btn-outline-custom">Our Services</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="image-container" @if (isset($homesection->background_image)) style="background-image: url(' {{asset('images/'.$homesection->background_image)}} ');" @else style="background-image: url(' {{asset('files/images/header_image1.png')}} ');" @endif>
                        <div class="row">
                            <div class="col-6">
                                <canvas id="canvas1" width="400" height="400">
                                </canvas>
                            </div>
                            <div class="col-6">
                                <a href="#" class="btn btn-light btn-sm menu-bar"><i class="fas fa-bars"></i></a>
                                <figure>
                                    <img class="veechi-logo d-none" src="{{ asset('files/images/logo02.png') }}" alt="" class="img-responsive">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 small-device d-none">
                        <div class="page-caption">
                            {{--  Company Moto  --}}
                            <h6 class="veechi-moto text-center">
                                @if (isset($homesection->company_moto))
                                    {{$homesection->company_moto}}
                                @else
                                    We solve UX problems to achive your business goal...!
                                @endif
                            </h6>
    
                            {{--  Company Name  --}}
                            <h1 class="veechi-welcome m-t-60">
                                welcome to <br>
                                @if (isset($homesection->company_name))
                                    <span class="veechi">{{$homesection->company_name}}</span>
                                @else
                                    <span class="veechi">Veechi <span class="tech">Technologies</span></span>
                                @endif
                            </h1>
    
                            {{--  Short Note  --}}
                            <p class="veechi-info">
                                @if (isset($homesection->short_note))
                                    {{$homesection->short_note}}
                                @else
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia aliquid labore architecto, quas quod quidem voluptatem perferendis blanditiis excepturi adipisci!
                                @endif
                            </p>
                            <div class="row p-t-50">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-6">
                                            <a href="" class="btn btn-success btn-lg btn-block btn-custom">Get Started</a>
                                        </div>
                                        <div class="col-6">
                                            <a href="" class="btn btn-outline-success btn-lg btn-block btn-outline-custom">Our Services</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach
    {{-- ================== < Carousel Part Ends>================== --}}

    
    {{--  =================< About Part Part Starts >=================  --}}
    @foreach ($aboutsections as $aboutsection)
    <section class="about-part p-t-50 p-b-70" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-70">
                        <div class="title-head">
                            <h1>ABOUT</h1>
                        </div>
                        <div class="title-caption">
                            <h3><span>who</span> we are..!!</h3>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 d-none d-sm-block">
                    <div class="about-img" @if (isset($aboutsection->about_image)) style="background-image: url(' {{asset('images/'.$aboutsection->about_image)}} ');" @else style="background-image: url(' {{asset('files/images/about_us.png')}} ');" @endif>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-us">
                        @if (isset($aboutsection->about_heading))
                            <h3>{{$aboutsection->about_heading}}</h3>
                        @else
                            <h3>We are a team</h3>
                        @endif

                        @if (isset($aboutsection->about_details))
                            <p class="text-justify text-muted">{{$aboutsection->about_details}}</p>
                        @else
                            <p class="text-justify text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere quis blanditiis, dignissimos officiis at eos ducimus maiores, maxime delectus laudantium voluptatum molestiae consequatur suscipit perspiciatis accusamus ex rem cumque. <br> Distinctio expedita odio doloremque quia. Libero, unde eos corrupti, iste molestias distinctio tempora dolorum deserunt atque vel dolor veritatis, minus placeat.</p>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="social p-t-40">
                                <ul>
                                    @if (isset($aboutsection->linkdin))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Linkdin"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->skype))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Skype"><i class="fab fa-skype"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->facebook))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->twitter))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->whatsapp))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Whatsapp"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->viber))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Viber"><i class="fab fa-viber"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->telegram))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Telegram"><i class="fab fa-telegram-plane"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->google_plus))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    @endforeach
    {{--  ==================< About Part Part Ends >==================  --}}


    {{--  =================< Service Part Starts >=================  --}}
    @foreach ($servicesections as $servicesection)
    <section class="service-part p-t-50 p-b-50" id="service">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-70">
                        <div class="title-head">
                            <h1>SERVICE</h1>
                        </div>
                        <div class="title-caption m-l-80">
                            <h3><span>what</span> we provide..!!</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                {{--  Service One  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_one_image))
                                    <img src="{{ asset('images/'.$servicesection->service_one_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_one_title))
                                <h4>{{$servicesection->service_one_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_one_details))
                                <p class="text-muted">{{$servicesection->service_one_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Two  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_two_image))
                                    <img src="{{ asset('images/'.$servicesection->service_two_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_two_title))
                                <h4>{{$servicesection->service_two_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_two_details))
                                <p class="text-muted">{{$servicesection->service_two_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Three  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_three_image))
                                    <img src="{{ asset('images/'.$servicesection->service_three_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_three_title))
                                <h4>{{$servicesection->service_three_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_three_details))
                                <p class="text-muted">{{$servicesection->service_three_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Four  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_four_image))
                                    <img src="{{ asset('images/'.$servicesection->service_four_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_four_title))
                                <h4>{{$servicesection->service_four_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_four_details))
                                <p class="text-muted">{{$servicesection->service_four_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Five  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_five_image))
                                    <img src="{{ asset('images/'.$servicesection->service_five_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_five_title))
                                <h4>{{$servicesection->service_five_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_five_details))
                                <p class="text-muted">{{$servicesection->service_five_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>
                
                {{--  Service Six  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_six_image))
                                    <img src="{{ asset('images/'.$servicesection->service_six_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_six_title))
                                <h4>{{$servicesection->service_six_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_six_details))
                                <p class="text-muted">{{$servicesection->service_six_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach
    {{--  ==================< Service Part Ends >==================  --}}


    {{--  =================< Team Part Starts >=================  --}}
    <section class="team-part p-t-50 p-b-50" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-50">
                        <div class="title-head">
                            <h1>TEAM</h1>
                        </div>
                        <div class="title-caption m-l-100 p-l-30">
                            <h3><span>our</span> team members..!!</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="our-team">
                        <div class="owl-carousel owl-theme">
                            {{--  @@foreach ($members as $member)  --}}
                            <div class="item team-member text-center">
                                <figure class="member-image">
                                    <img src="{{ asset('files/images/user.jpg') }}" alt="carousel-img">
                                </figure>
                                <div class="member-details">
                                    <h5 class="member-title m-t-30">Tanjil Ahmed Fahim</h5>
                                    <p class="member-designation text-muted">Cheif Executive Officer</p>
                                    <ul class="member-social-links">
                                        <li>
                                            <a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="Linkdin"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="Skype"><i class="fab fa-skype"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {{--  @endforeach  --}}
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--  ==================< Team Part Ends >==================  --}}


    {{--  =================< Wrok-Flow Part Starts >=================  --}}
    <section class="workflow-part p-t-50 p-b-50" id="workflow">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-50">
                        <div class="title-head">
                            <h1>WORKFLOW</h1>
                        </div>
                        <div class="title-caption text-right m-r-70">
                            <h3><span>working process</span> for our clients..!!</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6 col-md-3 offset-md-1">
                    <div class="workflow user-research text-center">
                        <figure>
                            <img src="{{ asset('files/images/01_user_research.png') }}" alt="user-research" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>01</sup> User Research</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1">
                    <div class="workflow ux-creation text-center">
                        <figure>
                            <img src="{{ asset('files/images/02_ux_creation.png') }}" alt="ux-creation" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>02</sup> UX Creation</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1">
                    <div class="workflow ux-implementation text-center">
                        <figure>
                            <img src="{{ asset('files/images/03_ux_implementation.png') }}" alt="ux-implementation" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>03</sup> UX Implementation</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 small-device d-md-none">
                    <div class="workflow ui-development text-center">
                        <figure>
                            <img src="{{ asset('files/images/04_ui_development.png') }}" alt="ui-development" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>04</sup> UI Development</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 small-device d-md-none">
                    <div class="workflow product-development text-center">
                        <figure>
                            <img src="{{ asset('files/images/05_product_development.png') }}" alt="product-development" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>05</sup> Product Development</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 small-device d-md-none">
                    <div class="workflow usability-testing text-center">
                        <figure>
                            <img src="{{ asset('files/images/06_usability_test.png') }}" alt="usability-testing" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>06</sup> Usability Testing</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 large-device">
                    <div class="workflow usability-testing text-center">
                        <figure>
                            <img src="{{ asset('files/images/06_usability_test.png') }}" alt="usability-testing" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>06</sup> Usability Testing</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 large-device">
                    <div class="workflow product-development text-center">
                        <figure>
                            <img src="{{ asset('files/images/05_product_development.png') }}" alt="product-development" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>05</sup> Product Development</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-6 col-md-3 offset-md-1 large-device">
                    <div class="workflow ui-development text-center">
                        <figure>
                            <img src="{{ asset('files/images/04_ui_development.png') }}" alt="ui-development" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>04</sup> UI Development</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                
                <div class="col-6 col-md-3 offset-md-5">
                    <div class="workflow project-handover text-center">
                        <figure>
                            <img src="{{ asset('files/images/07_project_handover.png') }}" alt="project-handover" width="50%" class="img-responsive">
                        </figure>
                        <h5><sup>07</sup> Project Handover</h5>
                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--  ==================< Wrok-Flow Part Ends >==================  --}}


    {{--  =================< Contact Part Starts >=================  --}}
    <section class="contact-part p-t-40 p-b-50" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-20">
                        <div class="title-head">
                            <h1>CONTACT</h1>
                        </div>
                        <div class="title-caption text-center m-l-100">
                            <h3><span>get in touch</span> with us</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="contact-us">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="contact-image">
                                    <figure>
                                        <img src="{{ asset('files/images/contact.png') }}" alt="" class="img-responsive">
                                    </figure>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form">
                                    <form action="" method="">
                                        <div class="form-group">
                                            <input name="name" type="text" class="form-control form-control-lg" id="name" placeholder="Name *">
                                        </div>
                                        <div class="form-group">
                                            <input name="email" type="email" class="form-control form-control-lg" id="email" placeholder="Email *">
                                        </div>
                                        <div class="form-group">
                                            <input name="subject" type="text" class="form-control form-control-lg" id="subject" placeholder="Subject *">
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" class="form-control form-control-lg" id="message" placeholder="Message *" rows="4"></textarea>
                                        </div>
                                        <div class="form-group contact-btn">
                                            <button class="btn btn-info btn-lg btn-msg text-center" type="submit">Send Message <i class="fas fa-arrow-right"></i></button>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--  ==================< Contact Part Ends >==================  --}}


    {{--  =================< Footer Part Starts >=================  --}}
    <footer class="footer-part p-t-40 p-b-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo-part text-center">
                        <figure><img src="{{ asset('files/images/logo02.png') }}" alt="" class="img-responsive"></figure>
                        <p class="text-justify text-muted">Veechi is a marketing communications agency which helps you stay one step ahead. We work as a marketing team to deliver strategic campaigns across all marketing platforms.</p>
                    </div>
                </div>
                <div class="col-3 col-md-2">
                    <div class="resources">
                        <h5>Rerources</h5>
                        <ul>
                            <li><a href="#home">Home</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#service">Services</a></li>
                            <li><a href="#workflow">Workflow</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-3 col-md-2">
                    <div class="important-links">
                        <h5>Important Links</h5>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Help Center</a></li>
                            <li><a href="#">Community</a></li>
                            <li><a href="#">Marketplace</a></li>
                            <li><a href="{{ route('frontend.career') }}">Career</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="connection">
                        <h5>Contact Us</h5>
                        <ul>
                            <li>
                                <i class="fas fa-map-marker-alt"></i>
                                2nd Floor (Optimum Tower), Lamabazar <span>Sylhet, Bangladesh</span>
                            </li>
                            <li>
                                <i class="fas fa-phone-volume"></i>
                                +880 1712 345678 <span>+880 1712 345679</span>
                            </li>
                            <li>
                                <i class="far fa-envelope"></i>
                                info@veechi.com
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    {{--  ==================< Footer Part Ends >==================  --}}


    {{--  =================< Bottom Part Starts >=================  --}}
    <section class="bottom-part p-t-20 p-b-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Design & Developed by Veechi Technologies</p>
                </div>
            </div>
        </div>
    </section>
    {{--  ==================< Bottom Part Ends >==================  --}}


    {{--  =================< Scroll-Top Part Starts >=================  --}}
    <div id="stop" class="scrollTop" style="opacity: 0;">
        <a href=""><i class="fas fa-chevron-up"></i></a>
    </div>
    {{--  ==================< Scroll-Top Part Ends >==================  --}}


    {{--  =================< Part Starts >=================  --}}

    {{--  ==================< Part Ends >==================  --}}
        
        
        
        
        
        
        
        
        
        
        {{-- Jquery JS Library --}}
        <script src="{{ asset('files/js/jquery/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('files/js/jquery/popper.min.js') }}" defer></script>

        {{-- Bootstrap JS --}}
        <script src="{{ asset('files/js/bootstrap/bootstrap.min.js') }}" defer></script>

        {{-- Smooth Scroll JS --}}
        {{--  <script src="{{ asset('files/js/smooth-scroll/smooth-scroll.js') }}" defer></script>  --}}

        {{--  Carousel JS  --}}
        <script src="{{ asset('files/js/owl-carousel/owl.carousel.min.js') }}" defer></script>

        {{-- Custom JS --}}
        <script src="{{ asset('files/js/script.js')}}"></script>
</body>
</html>