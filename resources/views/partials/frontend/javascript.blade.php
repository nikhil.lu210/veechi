{{-- Jquery JS Library --}}
<script src="{{ asset('files/js/jquery/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('files/js/jquery/popper.min.js') }}" defer></script>

{{-- Bootstrap JS --}}
<script src="{{ asset('files/js/bootstrap/bootstrap.min.js') }}" defer></script>

{{-- Smooth Scroll JS --}}
{{--  <script src="{{ asset('files/js/smooth-scroll/smooth-scroll.js') }}" defer></script>  --}}

{{--  Carousel JS  --}}
<script src="{{ asset('files/js/owl-carousel/owl.carousel.min.js') }}" defer></script>

@yield('scripts')

{{-- Custom JS --}}
{{-- <script src="{{ asset('files/js/script.js')}}"></script> --}}
</body>
</html>