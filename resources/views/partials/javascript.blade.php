{{--  Jquery JS Library  --}}
<script src="{{ asset('admin-files/js/jquery/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('admin-files/js/jquery/popper.min.js') }}" defer></script>

{{--  Bootstrap JS  --}}
<script src="{{ asset('admin-files/js/bootstrap/bootstrap.min.js') }}" defer></script>

{{--  Custom JS  --}}
<script src="{{ asset('admin-files/js/vali/plugins/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin-files/js/vali/plugins/jquery-ui.custom.min.js')}}"></script>
<script src="{{ asset('admin-files/js/vali/main.js')}}"></script>


{{-- <!-- The javascript plugin to display page loading on top--> --}}
{{-- PACE JS --}}
<script src="{{ asset('admin-files/js/vali/plugins/pace.min.js')}}"></script>

@yield('scripts')

{{--  Custom JS  --}}
<script src="{{ asset('admin-files/js/script.js')}}"></script>



</body>
</html>