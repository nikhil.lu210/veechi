{{-- <!-- Sidebar menu--> --}}
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <a href="{{ route('home') }}">
            <img class="app-sidebar__user-avatar" src="{{ asset('admin-files/images/user.jpg') }}" alt="User Image">
        </a>
        <div>
            <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
            <p class="app-sidebar__user-designation">Super Admin</p>
        </div>
    </div>
    <ul class="app-menu">
        {{-- Dashboard Menu --}}
        <li>
            <a class="app-menu__item" href="{{ route('home') }}">
                <i class="app-menu__icon fas fa-dice-d6"></i>
                <span class="app-menu__label">Dashboard</span>
            </a>
        </li>

        {{-- Website Menu --}}
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-project-diagram"></i><span class="app-menu__label">Website Sections</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{ route('homepage') }}"><i class="icon fas fa-home"></i> Home Section</a></li>

                <li><a class="treeview-item" href="{{ route('about') }}"><i class="icon fas fa-book-reader"></i> About Section</a></li>

                <li><a class="treeview-item" href="{{ route('service') }}"><i class="icon fas fa-hand-paper"></i> Service Section</a></li>

                <li><a class="treeview-item" href="{{ route('team') }}"><i class="icon fab fa-hornbill"></i> Team Section</a></li>

                {{--  <li><a class="treeview-item" href="{{ route('workflow') }}"><i class="icon fas fa-hourglass-half"></i> Workflow Section</a></li>  --}}

                <li><a class="treeview-item" href="{{ route('contact') }}"><i class="icon fas fa-comments"></i> Contact Section</a></li>

                <li><a class="treeview-item" href="{{ route('footer') }}"><i class="icon fas fa-atlas"></i> Footer Section</a></li>
            </ul>
        </li>

        {{-- Extra Menu --}}
        <li class="treeview"><a class="app-menu__item" href="" data-toggle="treeview"><i class="app-menu__icon far fa-file-alt"></i><span class="app-menu__label">Extra Pages</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li>
                    <a class="treeview-item" href="{{ route('blog') }}">
                        <i class="icon fas fa-rss"></i>
                        Blog
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="{{ route('help') }}">
                        <i class="icon far fa-question-circle"></i>
                        Help Center
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="{{ route('community') }}">
                        <i class="icon fab fa-connectdevelop"></i>
                        Community
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="{{ route('marketplace') }}">
                        <i class="icon fas fa-thumbtack"></i>
                        Marketplace
                    </a>
                </li>
                <li>
                    <a class="treeview-item" href="{{ route('career') }}">
                        <i class="icon fas fa-parachute-box"></i>
                        Career
                    </a>
                </li>
            </ul>
        </li>

        {{-- Homepage Menu
        <li>
            <a class="app-menu__item" href="#">
                <i class="app-menu__icon fas fa-home"></i>
                <span class="app-menu__label">Homepage</span>
            </a>
        </li> --}}

    </ul>
</aside>