@extends('layouts.frontend')

@section('page_title', '| CAREER')

@section('stylesheet')
    {{--  External CSS  --}}
    
    <link rel="stylesheet" href="{{ asset('admin-files/css/nice-select/nice-select.css') }}">
    
    <style>
        .career-header-part {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            min-height: 30vh;
            background-color: #ed1b24;
        }
        .heading-caption{
            position: relative;
            min-width: 50px;
            min-height: 90px;
            /*background-color: #fff;
            box-shadow: 0px 0px 40px 0px rgba(0, 0, 0, 0.05);*/
            -webkit-perspective: 800px;
            perspective: 800px;
            -webkit-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
        }
        .heading-caption h1{
            padding-top: 1%;
            text-transform: uppercase;
            color: #ffffff;
            font-size: 3rem;
        }
        .heading-caption .breadcrumb {
            display: -ms-flexbox;
            display: inline-flex;
            -ms-flex-wrap: wrap;
            flex-wrap: nowrap;
            padding: 0;
            margin-bottom: 1rem;
            margin-left: 20px;
            list-style: none;
            background-color: transparent;
            border-radius: 0;
        }
        .heading-caption .breadcrumb li{
            font-weight: bold;
            text-transform: capitalize;
        }
        .heading-caption .breadcrumb li a{
            color: #ffffff;
            font-weight: bold;
            outline: none;
        }
        .heading-caption .breadcrumb li a:hover,
        .heading-caption .breadcrumb li a:active,
        .heading-caption .breadcrumb li a:focus{
            text-decoration: none;
            outline: none;
        }
        .heading-caption .breadcrumb-item.active{
            color: #ffffff;
            opacity: 0.9;
        }
        .heading-caption .breadcrumb-item+.breadcrumb-item::before{
            color: #ffffff;
            content: '/';
        }

        .card.border-light {
            border-color: #ed1b24!important;
        }
        .card-header, .card-footer{
            background-color: #ed1b24;
        }
        .card-header h4{
            margin-bottom: 0px;
            text-transform: uppercase;
            font-weight: bold;
            color: #ffffff;
        }
        .btn-default{
            background: #ffffff;
            font-weight: bold;
            color: #333333;
            outline: none !important;
            transition: 0.3s all ease-in-out;
        }
        .btn-default:hover,
        .btn-default:focus,
        .btn-default:active{
            color: #000000;
            outline: none !important;
            box-shadow: none !important;
            transition: 0.3s all ease-in-out;
        }

        label{
            font-weight: 500;
        }
        .form-control {
            height: calc(2.25rem + 10px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small {
            font-weight: 700;
            font-size: 14px;
            height: calc(2.25rem + 10px);
            line-height: 44px;
            border-color: #dddddd;
            transition: 0.5s all ease-in-out;
        }
        .nice-select:active, 
        .nice-select.open, 
        .nice-select:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small .option {
            font-weight: 500;
        }

        .titles{
            font-weight: 900;
        }
        ul.infos{
            font-weight: 500;
            color: #333333;
        }
        tr th{
            font-weight: 900;
        }
        tr td{
            font-weight: 500;
            color: #333333;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Heading Part Starts >======== --}}
    <section class="career-header-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-caption text-center">
                        <h1>Designer Required</h1>
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('index') }}">HOME</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('frontend.career') }}">CAREER</a></li>
                            <li class="breadcrumb-item active">Designer Required</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Heading Part Ends >========= --}}
    

    {{-- ========< Job Post Part Starts >======== --}}
    <section class="job-post-part">
        <div class="container">
            <div class="row job-details m-b-40">
                <div class="col-md-8">
                    <div class="job-post m-t-40">
                        <div class="card border-light">
                            <div class="card-header border-light">
                                <h4>Designer Required</h4>
                            </div>
                            <div class="card-body border-light">
                                <div class="education">
                                    <h6 class="titles">Educational Requirements</h6>
                                    <ul class="infos p-l-40">
                                        <li>HSC</li>
                                        <li>Minimum HSC or Equivalent.</li>
                                        <li>Bachelor of Fine Arts/ Masters of Fine Arts would be preferable.</li>
                                        <li>Training/Trade Course: Graphics Design</li>
                                    </ul>
                                </div>
                                <div class="responsibilities">
                                    <h6 class="titles">Job Responsibilities</h6>
                                    <ul class="infos p-l-40">
                                        <li>Create innovative and world-class designs for the hungry international market.</li>
                                        <li>Research current design, graphics trends, color, and technology in Modern Print, web and mobile application sector.</li>
                                        <li>Designing brand collaterals as Logo, Color scheme and design guidelines</li>
                                    </ul>
                                </div>
                                <div class="experience">
                                    <h6 class="titles">Experience Requirements</h6>
                                    <ul class="infos p-l-40">
                                        <li>Create innovative and world-class designs for the hungry international market.</li>
                                        <li>Research current design, graphics trends, color, and technology in Modern Print, web and mobile application sector.</li>
                                        <li>Designing brand collaterals as Logo, Color scheme and design guidelines</li>
                                    </ul>
                                </div>
                                <div class="benefits">
                                    <h6 class="titles">Compensation & Other Benefits</h6>
                                    <ul class="infos p-l-40">
                                        <li>Lunch Facilities: Partially Subsidize</li>
                                        <li>Salary Review: Yearly</li>
                                        <li>Festival Bonus: 2(Yearly)</li>
                                        <li>Training on new technology.</li>
                                        <li>Most importantly a friendly work environment with opportunity to learn from a number of highly skilled mentors.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-footer border-light">
                                <button class="btn btn-default btn-apply float-right" id="applyBtn"> <i class="far fa-thumbs-up"></i> APPLY</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="job-summary m-t-40">
                        <div class="card border-light">
                            <div class="card-header border-light">
                                <h4>Job Summary</h4>
                            </div>
                            <div class="card-body border-light">
                                <div class="summary">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Published:</th>
                                                <td style="text-align: right !important;">12-12-2018</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Deadline:</th>
                                                <td style="text-align: right !important;">31-12-2018</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Status:</th>
                                                <td style="text-align: right !important;">Fulltime</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Experience:</th>
                                                <td style="text-align: right !important;">At List 2 Years</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Gender:</th>
                                                <td style="text-align: right !important;">Male/Female</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Salary:</th>
                                                <td style="text-align: right !important;">N/A</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Location:</th>
                                                <td style="text-align: right !important;">Sylhet</td>
                                            </tr>
                                            <tr>
                                                <th style="text-align: left !important; width:40%;">Age:</th>
                                                <td style="text-align: right !important;">20 to 30 years</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="apply-form m-t-40 m-b-40" style="display: none;">
                        <div class="card border-light">
                            <div class="card-header border-light">
                                <h4>Apply For This Job</h4>
                            </div>
                            <div class="card-body border-light">
                                <form action="" method="">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">First Name*</label>
                                                <input class="form-control" type="text" placeholder="Jhon">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Last Name*</label>
                                                <input class="form-control" type="text" placeholder="Doe">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Gender*</label><br>
                                                <select class="small wide">
                                                    <option data-display="Select Gender">Select Gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Email*</label>
                                                <input class="form-control" type="email" placeholder="jhon@mail.com">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Mobile No.*</label>
                                                <input class="form-control" type="text" placeholder="+88012345678">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Age*</label>
                                                <input class="form-control" type="text" placeholder="25">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Expected Salary*</label>
                                                <input class="form-control" type="text" placeholder="20000">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Experience* <sup>(in year)</sup></label>
                                                <input class="form-control" type="text" placeholder="3">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">CV / Resume* <sup>(.doc / .pdf)</sup></label>
                                                <input class="form-control" type="file">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Address*</label>
                                                <textarea class="form-control" name="address" id="address" rows="3" placeholder="Your Full Address Here."></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Bio.*</label>
                                                <textarea class="form-control" name="bio" id="bio" rows="3" placeholder="Introduce Yourself Here."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer border-light">
                                <button type="submit" class="btn btn-default btn-apply float-right" id="applyNowBtn">APPLY NOW <i class="fab fa-telegram-plane"></i></button>
                                <button class="btn btn-default btn-cancle float-right m-r-10" id="cancleBtn"><i class="far fa-times-circle"></i> CANCLE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Job Post Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('admin-files/js/nice-select/jquery.nice-select.min.js') }}"></script>

    <script>
        $(function(){
            $("#applyBtn").click(function(){
                $('#cancleBtn').removeClass('d-none');
                $('#applyBtn').addClass('d-none');
                $('#rejectBtn').addClass('d-none');
                $('.job-details').addClass('d-none');
            });
            $("#cancleBtn").click(function(){
                $('#applyBtn').removeClass('d-none');
                $('#cancleBtn').addClass('d-none'); 
                $('#rejectBtn').removeClass('d-none');      
                $('.job-details').removeClass('d-none');
            });
            $("#applyBtn, #cancleBtn").click(function(){
                $(".apply-form").slideToggle();
                return false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('select').niceSelect();
        });
    </script>
@endsection