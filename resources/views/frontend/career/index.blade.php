@extends('layouts.frontend')

@section('page_title', '| CAREER')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .career-header-part {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            min-height: 50vh;
            background-color: #ed1b24;
        }
        .heading-caption{
            position: relative;
            min-width: 50px;
            min-height: 90px;
            /*background-color: #fff;
            box-shadow: 0px 0px 40px 0px rgba(0, 0, 0, 0.05);*/
            -webkit-perspective: 800px;
            perspective: 800px;
            -webkit-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
        }
        .heading-caption h1{
            padding-top: 1%;
            text-transform: uppercase;
            color: #ffffff;
            font-size: 6rem;
        }
        .heading-caption .breadcrumb {
            display: -ms-flexbox;
            display: inline-flex;
            -ms-flex-wrap: wrap;
            flex-wrap: nowrap;
            padding: 0;
            margin-bottom: 1rem;
            margin-left: 20px;
            list-style: none;
            background-color: transparent;
            border-radius: 0;
        }
        .heading-caption .breadcrumb li{
            font-weight: bold;
        }
        .heading-caption .breadcrumb li a{
            color: #ffffff;
            font-weight: bold;
            outline: none;
        }
        .heading-caption .breadcrumb li a:hover,
        .heading-caption .breadcrumb li a:active,
        .heading-caption .breadcrumb li a:focus{
            text-decoration: none;
            outline: none;
        }
        .heading-caption .breadcrumb-item.active{
            color: #ffffff;
            opacity: 0.9;
        }
        .heading-caption .breadcrumb-item+.breadcrumb-item::before{
            color: #ffffff;
            content: '/';
        }

        .career-post{
            padding: 20px 5px;
            border: 1px solid #dddddd40;
            border-radius: 8px;
            box-shadow: 0px 0px 32px 0px rgba(0,0,0,0.02);
            transition: 0.3s all ease-in-out;
        }
        .career-post:hover{
            border: 1px solid #ffffff;
            box-shadow: 0px 0px 32px 0px rgba(0,0,0,0.05);
            transition: 0.3s all ease-in-out;
        }
        .career-post .img-responsive{
            border-color: #ffffff;
        }
        .post-details .post-title{
            color: #333333;
            font-weight: bold;
            transition: 0.3s all ease-in-out;
        }
        .post-details .post-title:hover{
            text-decoration: none;
            color: #ed1b24;
            transition: 0.3s all ease-in-out;
        }
        .post-element {
            display: inline;
        }
        .post-element li{
            font-size: 14px;
            display: inline;
            padding-right: 15px;
            font-weight: 500;
            color: #666666;
        }
        .post-element li.job-type i{
            color: #ed1b24;
            opacity: 0.6;
        }
        .post-element li.job-salary i{
            color: #28b573;
            opacity: 0.6;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Heading Part Starts >======== --}}
    <section class="career-header-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading-caption text-center">
                        <h1>Career</h1>
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('index') }}">HOME</a></li>
                            <li class="breadcrumb-item active">CAREER</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Heading Part Ends >========= --}}
    

    {{-- ========< Career Part Starts >======== --}}
    <section class="career-part m-t-40">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="career-post m-b-30">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="post-img">
                                    <a href="{{ route('frontend.career.show',1) }}">
                                        <img src="{{ asset('files/images/01_user_research.png') }}" alt="" class="img-thumbnail img-responsive">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="post-details p-t-15">
                                    <h5><a href="{{ route('frontend.career.show',1) }}" class="post-title">Designer Required</a></h5>
                                    <ul class="post-element">
                                        <li class="job-type"><i class="far fa-clock"></i> Fulltime</li>
                                        <li class="job-salary"><i class="fas fa-hand-holding-usd"></i> 25000</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Career Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    
@endsection