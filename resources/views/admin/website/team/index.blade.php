@extends('layouts.main')

@section('page_title', '| TEAM')

@section('stylesheet')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('files/css/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('files/css/owl-carousel/owl.theme.default.css') }}">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .team-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
            }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }
        
        .title-div{
            font-family: 'Quicksand', sans-serif;
        }
        .title-div .title-head h1{
            opacity: 0.1; 
            font-weight: 900; 
            color: #ed1b24;
            font-size: 72px;
            margin-left: -30px;
        }
        .title-div .title-caption h3{
            margin-top: -20px;
            opacity: 1;
            font-weight: 700;
            font-size: 32px;
            margin-top: -40px;
            margin-left: 160px;
        }
        .title-div .title-caption h3 span{
            font-weight: 400;
            color: #ed1b24;
        }

        .team-part .team-member{
            background-color: #fff;
            border-radius: 10px;
            border-top: 3px solid #FFF;
            border-bottom: 3px solid #e3e3e3;
            transition: 0.3s all ease-in-out;
        }
        .team-part .team-member:hover{
            background-color: #fff;
            cursor: pointer;
            border-radius: 10px;
            border-top: 3px solid #e3e3e3;
            border-bottom: 3px solid #FFF;
            transition: 0.3s all ease-in-out;
        }
        
        .team-part .team-member .member-image{
            display: block;
            margin-right: auto;
            margin-left: auto;
            padding: 20px 50px 0px;
        }
        .team-part .team-member .member-image img{
            border-radius: 100%;
            transform: scale(0.9);
            transition: 0.3s all ease-in-out;
            /* border: 3px solid; */
        }
        .team-part .team-member:hover .member-image img{
            transform: scale(1);
            transition: 0.3s all ease-in-out;
        }
        .team-part .team-member .member-details .member-title{
            font-weight: bold;
            margin-bottom: 0px;
            transition: 0.3s all ease-in-out;
        }
        
        .team-part .team-member .member-details .member-title a{
            color: #000000;
            text-decoration: none;
        }
        
        .team-part .team-member:hover .member-title a{
            color: #ed1b23;
            transition: 0.3s all ease-in-out;
        }
        .team-part .team-member .member-details .member-designation{
            font-size: 14px;
            font-weight: 500;
        }
        .team-part .member-social-links{
            padding-bottom: 20px;
        }
        .team-part .member-social-links li{
            /* float: left; */
            /* width: auto; */
            display: inline;
            margin: 0 0 0 15px;
            list-style: none;
        }
        .team-part .member-social-links li:nth-child(1){
            margin-left: 0px;
        }
        .team-part .member-social-links li a{
            font-size: 12px;
            border: 1px solid #666;
            border-radius: 3px;
        }
        .team-part .member-social-links li:nth-child(1) a{
            padding: 4px 9px;
            border-color: #3b5999;
            color: #3b5999;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(1) a:hover{
            background-color: #3b5999;
            color: #FFF;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(2) a{
            padding: 4px 7px;
            border-color: #0077B5;
            color: #0077B5;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(2) a:hover{
            background-color: #0077B5;
            color: #FFF;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(3) a{
            padding: 4px 6px;
            border-color: #55acee;
            color: #55acee;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(3) a:hover{
            background-color: #55acee;
            color: #FFF;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(4) a{
            padding: 4px 7px;
            border-color: #00AFF0;
            color: #00AFF0;
            transition: 0.3s all ease-in-out;
        }
        .team-part .member-social-links li:nth-child(4) a:hover{
            background-color: #00AFF0;
            color: #FFF;
            transition: 0.3s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fab fa-hornbill"></i> <b>TEAM</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">Team Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Team Part Starts >======== --}}
    <section class="team-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="edit-page">
                        <a href="{{ route('team.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Add New Member"><i class="fas fa-plus"></i></a>
                         <a href="{{ route('team.show', 1) }}" class="btn btn-light btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="View All Team Member"><i class="fas fa-pencil-alt"></i></a> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-30">
                        <div class="title-head">
                            <h1>TEAM</h1>
                        </div>
                        <div class="title-caption m-l-100 p-l-30">
                            <h3><span>our</span> team members..!!</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="our-team">
                        <div class="owl-carousel owl-theme">
                            @foreach ($teamsections as $team) 
                            <div class="item team-member text-center">
                                <figure class="member-image">
                                    @if (isset($team->profile_picture))
                                        <img src="{{ asset('images/'.$team->profile_picture) }}" alt="carousel-img">
                                    @else
                                        <img src="{{ asset('files/images/user.jpg') }}" alt="carousel-img">
                                    @endif
                                </figure>
                                <div class="member-details">
                                    {{-- Team Member Name --}}
                                    @if (isset($team->name))
                                        <h5 class="member-title m-t-30">
                                            <a href="#">{{$team->name}}</a>
                                        </h5>
                                    @else
                                        <h5 class="member-title m-t-30">
                                            <a href="#">Member Name</a>
                                        </h5>
                                    @endif

                                    {{-- Team member Designation --}}
                                    @if (isset($team->designation))
                                        <p class="member-designation text-muted">{{$team->designation}}</p>
                                    @else
                                        <p class="member-designation text-muted">Designation</p>
                                    @endif
                                    
                                    <ul class="member-social-links">
                                        <li>
                                            <a href="{{$team->facebook}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="{{$team->linkdin}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Linkdin"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                        <li>
                                            <a href="{{$team->twitter}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="{{$team->skype}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Skype"><i class="fab fa-skype"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                             @endforeach 
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Team Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    {{--  Carousel JS  --}}
    <script src="{{ asset('files/js/owl-carousel/owl.carousel.min.js') }}" defer></script>

    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                // items: 4,
                margin: 7,
                loop: true,
                dots: false,
                nav: false,
                // navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                autoplay: true,
                smartSpeed: 1200,
                autoplayTimeout: 3000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:3
                    }
                }
            })
        });
    </script>
    
@endsection