@extends('layouts.main')

@section('page_title', '| Member Add')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
        }
        .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
        }
        .avatar-upload .avatar-edit input {
        display: none;
        }
        .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label i.fas.fa-pencil-alt {
            padding: 9px;
        }
        .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fab fa-hornbill"></i> <b>ADD TEAM MEMBER</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item"><a href="{{ route('team') }}">Team Section</a></li>
                <li class="breadcrumb-item active">Add Member</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Homepage Part Starts >======== --}}
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-right">
                                <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <form action="{{ Route('team.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5" style="border-right: 1px solid #efefef;">
                                        {{--  Team Member Image  --}}
                                        <div class="group">
                                            <div class="avatar-upload about">
                                                <div class="avatar-edit">
                                                    <input type='file' name="profile_picture" id="memberImageUpload" accept=".png, .jpg, .jpeg" />
                                                    <label for="memberImageUpload"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="memberImagePreview" style="background-image: url(' {{asset('files/images/about_us.png')}} '); background-size: 90%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--  About Right Part  --}}
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label">Team Member Name</label>
                                            <input name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" placeholder="Tanjil Ahmed Fahim">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Designation</label>
                                            <input name="designation" class="form-control {{ $errors->has('designation') ? ' is-invalid' : '' }}" type="text" placeholder="Chief Executive Officer">
                                            @if ($errors->has('designation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('designation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Facebook</label>
                                            <input name="facebook" class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}" type="text" placeholder="https://www.facebook.com/tanjil.fahim">
                                            @if ($errors->has('facebook'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('facebook') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Linkdin</label>
                                            <input name="linkdin" class="form-control {{ $errors->has('linkdin') ? ' is-invalid' : '' }}" type="text" placeholder="https://www.linkedin.com/in/tanjil.fahim">
                                            @if ($errors->has('linkdin'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('linkdin') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Twitter</label>
                                            <input name="twitter" class="form-control {{ $errors->has('twitter') ? ' is-invalid' : '' }}" type="text" placeholder="https://twitter.com/tanjil.fahim">
                                            @if ($errors->has('twitter'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('twitter') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Skype</label>
                                            <input name="skype" class="form-control {{ $errors->has('skype') ? ' is-invalid' : '' }}" type="text" placeholder="tanjil.fahim">
                                            @if ($errors->has('skype'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('skype') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="submit" class="btn btn-light custom-btn"><i class="far fa-thumbs-up"></i> Add Now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {{-- =========< Homepage Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        //For Changing About Image
        $(".avatar-upload.about").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#memberImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#memberImagePreview').hide();
                        $('#memberImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#memberImageUpload").change(function() {
                readURL(this);
            });
        });
    </script>
@endsection