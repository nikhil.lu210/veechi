@extends('layouts.main')

@section('page_title', '| SERVICE')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .service-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
            }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }

        .title-div{
            font-family: 'Quicksand', sans-serif;
        }
        .title-div .title-head h1{
            opacity: 0.1; 
            font-weight: 900; 
            color: #ed1b24;
            font-size: 72px;
            margin-left: -30px;
        }
        .title-div .title-caption h3{
            margin-top: -20px;
            opacity: 1;
            font-weight: 700;
            font-size: 32px;
            margin-top: -40px;
            margin-left: 160px;
        }
        .title-div .title-caption h3 span{
            font-weight: 400;
            color: #ed1b24;
        }
        .service-part .services{
            padding: 20px 20px;
            min-height: 320px;
            max-height: 320px;
            border-radius: 10px;
            margin-bottom: 30px;
            border-top: 3px solid #fff;
            border-bottom: 3px solid #e3e3e3;
            box-shadow: 0px 0px 32px 0px rgba(0,0,0,0.05);
            transition: 0.3s all ease-in-out;
        }
        .service-part .services:hover{
            border-radius: 20px;
            border-bottom: 3px solid #fff;
            border-top: 3px solid #e3e3e3;
            box-shadow: 0px 0px 32px 5px rgba(0,0,0,0.08);
            transition: 0.3s all ease-in-out;
            cursor: pointer;
        }
        .service-part .services .icon img{
            max-width: 100px;
            padding: 10px;
            opacity: 0.8;
            transition: 0.3s all ease-in-out;
        }
        .service-part .services:hover .icon img{
            opacity: 1;
            transition: 0.3s all ease-in-out;
        }
        .service-part .services .icon::before{
            content: " ";
            position: relative;
            background-color: #8a8a8a1f;
            padding: 20px 70px;
            top: 95px;
            left: -2px;
            border-radius: 80% 40% 80% 40%;
            transition: 0.3s all ease-in-out;
        }
        .service-part .services:hover .icon::before{
            background-color: #ed1b241a;
            border-radius: 40% 80% 40% 80%;
            transition: 0.3s all ease-in-out;
        } 
        .service-part .services .service-title h4{
            padding-top: 20px;
            opacity: 0.8;
            font-weight: bold;
            transition: 0.3s all ease-in-out;
        }
        .service-part .services:hover .service-title h4{
            color: #ed1b24;
            opacity: 1;
            transition: 0.3s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-hand-paper"></i><b> SERVICE </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('service') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">Service Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Service Part Starts >======== --}}
    <section class="service-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @foreach ($servicesections as $servicesection)
                    <div class="edit-page">
                        @if (isset($servicesection->id))
                            <a href="{{ route('service.create') }}" class="btn btn-light btn-sm btn-create m-l-10 disabled" data-toggle="tooltip" data-placement="top" title="Create Service"><i class="fas fa-plus"></i></a>                            
                        @else
                            <a href="{{ route('service.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Create Service"><i class="fas fa-plus"></i></a>
                        @endif

                        @if (isset($servicesection->id))
                            <a href="{{ route('service.show', $servicesection->id) }}" class="btn btn-light btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Update Service"><i class="fas fa-pencil-alt"></i></a>
                        @else
                            <a href="{{ route('service.show', $servicesection->id) }}" class="btn btn-light btn-sm btn-edit disabled" data-toggle="tooltip" data-placement="top" title="Update Service"><i class="fas fa-pencil-alt"></i></a>
                        @endif
                        
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-20">
                        <div class="title-head">
                            <h1>SERVICE</h1>
                        </div>
                        <div class="title-caption m-l-80">
                            <h3><span>what</span> we provide..!!</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                {{--  Service One  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_one_image))
                                    <img src="{{ asset('images/'.$servicesection->service_one_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_one_title))
                                <h4>{{$servicesection->service_one_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_one_details))
                                <p class="text-muted">{{$servicesection->service_one_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Two  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_two_image))
                                    <img src="{{ asset('images/'.$servicesection->service_two_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_two_title))
                                <h4>{{$servicesection->service_two_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_two_details))
                                <p class="text-muted">{{$servicesection->service_two_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Three  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_three_image))
                                    <img src="{{ asset('images/'.$servicesection->service_three_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_three_title))
                                <h4>{{$servicesection->service_three_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_three_details))
                                <p class="text-muted">{{$servicesection->service_three_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Four  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_four_image))
                                    <img src="{{ asset('images/'.$servicesection->service_four_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_four_title))
                                <h4>{{$servicesection->service_four_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_four_details))
                                <p class="text-muted">{{$servicesection->service_four_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>

                {{--  Service Five  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_five_image))
                                    <img src="{{ asset('images/'.$servicesection->service_five_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_five_title))
                                <h4>{{$servicesection->service_five_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_five_details))
                                <p class="text-muted">{{$servicesection->service_five_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>
                
                {{--  Service Six  --}}
                <div class="col-6 col-md-4">
                    <div class="services text-center">
                        <div class="icon">
                            <figure>
                                @if (isset($servicesection->service_six_image))
                                    <img src="{{ asset('images/'.$servicesection->service_six_image) }}" alt="">
                                @else
                                    <img src="{{ asset('files/images/icons/web_backend.png') }}" alt="">
                                @endif
                            </figure>
                        </div>
                        <div class="service-title">
                            @if (isset($servicesection->service_six_title))
                                <h4>{{$servicesection->service_six_title}}</h4>
                            @else
                                <h4>Service Title</h4>
                            @endif
                        </div>
                        <div class="details text-center">
                            @if (isset($servicesection->service_six_details))
                                <p class="text-muted">{{$servicesection->service_six_details}}</p>
                            @else
                                <p class="text-muted">Service Details</p>
                            @endif                            
                        </div>
                    </div>
                </div>
            </div> 
            @endforeach
            
        </div>
    </section>
    {{-- =========< Home Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection