@extends('layouts.main')

@section('page_title', '| Service Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
        }
        .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
        }
        .avatar-upload .avatar-edit input {
        display: none;
        }
        .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label i.fas.fa-pencil-alt {
            padding: 9px;
        }
        .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-hand-paper"></i><b> CREATE SERVICE </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item"><a href="{{ route('service') }}">Service Section</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Service Part Starts >======== --}}
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-right">
                                <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>

                        {{--  Form Starts Here  --}}
                        <form action="{{ Route('service.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6" style="border-right: 1px solid #efefef;">
                                        {{--  First Image  --}}
                                        <div class="group">
                                            <div class="avatar-upload first-image">
                                                {{--  service_one_image  --}}
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_one_image" id="firstImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="firstImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="firstImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_one_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_one_title" class="form-control {{ $errors->has('service_one_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_one_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_one_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_one_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_one_details" class="form-control {{ $errors->has('service_one_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_one_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_one_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    {{--  Second Image  --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            {{--  service_two_image  --}}
                                            <div class="avatar-upload second-image">
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_two_image" id="secondImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="secondImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="secondImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_two_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_two_title" class="form-control {{ $errors->has('service_two_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_two_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_two_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_two_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_two_details" class="form-control {{ $errors->has('service_two_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_two_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_two_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row m-t-40">
                                    <div class="col-md-6" style="border-right: 1px solid #efefef;">
                                        {{--  Third Image  --}}
                                        <div class="group">
                                            {{--  service_three_image  --}}
                                            <div class="avatar-upload third-image">
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_three_image" id="thirdImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="thirdImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="thirdImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_three_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_three_title" class="form-control {{ $errors->has('service_three_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_three_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_three_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_three_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_three_details" class="form-control {{ $errors->has('service_three_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_three_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_three_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    {{--  Fourth Image  --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            {{--  service_four_image  --}}
                                            <div class="avatar-upload fourth-image">
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_four_image" id="fourthImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="fourthImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="fourthImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_four_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_four_title" class="form-control {{ $errors->has('service_four_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_four_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_four_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_four_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_four_details" class="form-control {{ $errors->has('service_four_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_four_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_four_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row m-t-40">
                                    <div class="col-md-6" style="border-right: 1px solid #efefef;">
                                        {{-- Fifth Image  --}}
                                        <div class="group">
                                            {{--  service_five_image  --}}
                                            <div class="avatar-upload fifth-image">
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_five_image" id="fifthImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="fifthImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="fifthImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_five_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_five_title" class="form-control {{ $errors->has('service_five_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_five_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_five_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_five_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_five_details" class="form-control {{ $errors->has('service_five_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_five_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_five_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    {{--  Sixth Image  --}}
                                    <div class="col-md-6">
                                        <div class="group">
                                            {{--  service_six_image  --}}
                                            <div class="avatar-upload sixth-image">
                                                <div class="avatar-edit">
                                                    <input type='file' name="service_six_image" id="sixthImage" accept=".png, .jpg, .jpeg" />
                                                    <label for="sixthImage"><i class="fas fa-pencil-alt"></i></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="sixthImagePreview" style="background-image: url(' {{asset('files/images/icons/web_backend.png')}} '); background-size: 60%;"></div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--  service_six_title  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Title</label>
                                            <input name="service_six_title" class="form-control {{ $errors->has('service_six_title') ? ' is-invalid' : '' }}" type="text" placeholder="Web Development">
                                            @if ($errors->has('service_six_title'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_six_title') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  service_six_details  --}}
                                        <div class="form-group">
                                            <label class="control-label">Service Details</label>
                                            <textarea name="service_six_details" class="form-control {{ $errors->has('service_six_details') ? ' is-invalid' : '' }}" rows="2" placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. amet consectetur adipisicing elit."></textarea>
                                            @if ($errors->has('service_six_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('service_six_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{--  Submit Button  --}}
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="submit" class="btn btn-light custom-btn"><i class="far fa-thumbs-up"></i> Add Now</button>
                                </div>
                            </div>

                        </form>{{--  Form Ends Here  --}}
                    </div>
                </div>
            </div>
        </div>
    {{--  </section>  --}}
    {{-- =========< Homepage Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        //For Changing First Image
        $(".avatar-upload.first-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#firstImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#firstImagePreview').hide();
                        $('#firstImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#firstImage").change(function() {
                readURL(this);
            });
        });
        //For Changing Second Image
        $(".avatar-upload.second-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#secondImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#secondImagePreview').hide();
                        $('#secondImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#secondImage").change(function() {
                readURL(this);
            });
        });
        //For Changing Third Image
        $(".avatar-upload.third-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#thirdImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#thirdImagePreview').hide();
                        $('#thirdImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#thirdImage").change(function() {
                readURL(this);
            });
        });
        //For Changing Fourth Image
        $(".avatar-upload.fourth-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#fourthImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#fourthImagePreview').hide();
                        $('#fourthImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#fourthImage").change(function() {
                readURL(this);
            });
        });
        //For Changing Fifth Image
        $(".avatar-upload.fifth-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#fifthImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#fifthImagePreview').hide();
                        $('#fifthImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#fifthImage").change(function() {
                readURL(this);
            });
        });
        //For Changing Sixth Image
        $(".avatar-upload.sixth-image").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#sixthImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#sixthImagePreview').hide();
                        $('#sixthImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#sixthImage").change(function() {
                readURL(this);
            });
        });

        
    </script>
@endsection