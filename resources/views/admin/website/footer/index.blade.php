@extends('layouts.main')

@section('page_title', '| FOOTER')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .footer-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
        }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }
        p{
            font-size: 16px;
        }

        .footer-part .logo-part img{
            max-width: 30%;
        }
        .footer-part .important-links h5,
        .footer-part .resources h5{
            font-weight: 900;
            padding-bottom: 20px;
        }
        .footer-part .important-links ul,
        .footer-part .resources ul{
            margin-bottom: 0;
        }
        .footer-part .important-links ul>li,
        .footer-part .important-links ul>li a,
        .footer-part .resources ul>li,
        .footer-part .resources ul>li a{
            font-size: 16px;
            list-style: none;
            font-weight: 500;
            text-decoration: none;
            color: #666;
            padding-bottom: 10px;
            transition: 0.5s all ease-in-out;
        }
        .footer-part .important-links ul>li a:hover,
        .footer-part .resources ul>li a:hover{
            color: #ed1b24;
            padding-left: 20px;
            transition: 0.5s all ease-in-out;
        }
        .footer-part .important-links ul>li a::after,
        .footer-part .resources ul>li a::after {
            left: 15px;
            margin-top: 11px;
            content: " ";
            height: 2px;
            position: absolute;
            background-color: #ed1b24;
            width: 0;
            transition: 0.5s all ease-in-out;
        }
        .footer-part .important-links ul>li a:hover::after,
        .footer-part .resources ul>li a:hover::after {
            width: 15px;
            transition: 0.5s all ease-in-out;
        }
        .footer-part .connection h5{
            font-weight: 900;
            padding-bottom: 20px;
        }
        .footer-part .connection ul li{
            list-style: none;
            font-weight: 500;
            color: #666666;
            padding-bottom: 20px;
        }
        .footer-part .connection ul>li i{
            border: 1px solid #aaaaaa30;
            padding: 7px 9px;
            border-radius: 3px;
            font-size: 12px;
            background: #f5f5f5;
            color: #251e20b8;
        }
        .footer-part .connection ul>li:nth-child(3) i{
            padding: 7px 8px;
        }
        .footer-part .connection ul>li span{
            display: block;
            padding-left: 35px;
        }
        /* ===============< Footer Part Ends >============== */
        
        
        /* ==============< Bottom Part Starts >============= */
        .bottom-part{
            background-color: #f5f5f5;
            box-shadow: 0px -5px 10px 0px rgba(0,0,0,0.07);
        }
        .bottom-part p{
            margin-bottom: 0px;
            font-weight: 700;
        }
        /* ===============< Bottom Part Ends >============== */
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-atlas"></i> <b>FOOTER </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a>
                </li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">Footer Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{--  =================< Footer Part Starts >=================  --}}
    <footer class="footer-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="edit-page">
                        <a href="{{ route('footer.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Create Footer"><i class="fas fa-plus"></i></a>
                        <a href="{{ route('footer.show', 1) }}" class="btn btn-light btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Update Footer"><i class="fas fa-pencil-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="row p-t-20">
                <div class="col-md-4">
                    <div class="logo-part text-center">
                        <figure><img src="{{ asset('files/images/logo02.png') }}" alt="" class="img-responsive"></figure>
                        <p class="text-justify text-muted">Veechi is a marketing communications agency which helps you stay one step ahead. We work as a marketing team to deliver strategic campaigns across all marketing platforms.</p>
                    </div>
                </div>
                <div class="col-3 col-md-2">
                    <div class="resources">
                        <h5>Rerources</h5>
                        <ul>
                            <li><a href="#home">Home</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#service">Services</a></li>
                            <li><a href="#workflow">Workflow</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-3 col-md-2">
                    <div class="important-links">
                        <h5>Important Links</h5>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Help Center</a></li>
                            <li><a href="#">Community</a></li>
                            <li><a href="#">Marketplace</a></li>
                            <li><a href="#">Career</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="connection">
                        <h5>Contact Us</h5>
                        <ul>
                            <li>
                                <i class="fas fa-map-marker-alt"></i>
                                2nd Floor (Optimum Tower), Lamabazar <span>Sylhet, Bangladesh</span>
                            </li>
                            <li>
                                <i class="fas fa-phone-volume"></i>
                                +880 1712 345678 <span>+880 1712 345679</span>
                            </li>
                            <li>
                                <i class="far fa-envelope"></i>
                                info@veechi.com
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    {{--  ==================< Footer Part Ends >==================  --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection