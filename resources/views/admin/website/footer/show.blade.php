@extends('layouts.main')

@section('page_title', '| Footer Update')

@section('stylesheet')
    {{--  External CSS  --}}
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-home"></i> <b>DASHBOARD</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fas fa-home fa-lg"></i></li>
                <li class="breadcrumb-item active">Dashboard</li>
                {{-- <li class="breadcrumb-item"><a href="#">Dashboard</a></li> --}}
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Part Starts >======== --}}
    
    {{-- =========< Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
@endsection