@extends('layouts.main')

@section('page_title', '| ABOUT')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
        
        
        .about-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
          }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }

        .title-div{
            font-family: 'Quicksand', sans-serif;
        }
        .title-div .title-head h1{
            opacity: 0.1; 
            font-weight: 900; 
            color: #ed1b24;
            font-size: 72px;
            margin-left: -30px;
        }
        .title-div .title-caption h3{
            margin-top: -20px;
            opacity: 1;
            font-weight: 700;
            font-size: 32px;
            margin-top: -40px;
            margin-left: 160px;
        }
        .title-div .title-caption h3 span{
            font-weight: 400;
            color: #ed1b24;
        }
        
        .social ul>li{
            list-style: none;
            display: inline;
            margin-right: 10px;
            margin-left: 10px;
        }
        .social ul>li a{
            border: 1px solid;
            border-radius: 3px;
            transition: 0.3s all ease-in-out;
        }
        .social ul>li a:hover{
            border: 1px solid;
            transition: 0.3s all ease-in-out;
        }
        .social ul>li:nth-child(1) a{
            padding: 10px 13px;
            border-color: #0077B5;
            color: #0077B5;
        }
        .social ul>li:nth-child(1) a:hover{
            background-color: #0077B5;
            border-color: #0077B5;
            color: #fff;
        }
        .social ul>li:nth-child(2) a{
            padding: 10px 13px;
            border-color: #00AFF0;
            color: #00AFF0;
        }
        .social ul>li:nth-child(2) a:hover{
            background-color: #00AFF0;
            border-color: #00AFF0;
            color: #fff;
        }
        .social ul>li:nth-child(3) a{
            padding: 10px 16px;
            border-color: #3b5999;
            color: #3b5999;
        }
        .social ul>li:nth-child(3) a:hover{
            background-color: #3b5999;
            border-color: #3b5999;
            color: #fff;
        }
        .social ul>li:nth-child(4) a{
            padding: 10px 12px;
            border-color: #55acee;
            color: #55acee;
        }
        .social ul>li:nth-child(4) a:hover{
            background-color: #55acee;
            border-color: #55acee;
            color: #fff;
        }
        .social ul>li:nth-child(5) a{
            padding: 10px 13px;
            font-weight: bold;
            border-color: #25D366;
            color: #25D366;
        }
        .social ul>li:nth-child(5) a:hover{
            background-color: #25D366;
            border-color: #25D366;
            color: #fff;
        }
        .social ul>li:nth-child(6) a{
            padding: 10px 12px;
            font-weight: bold;
            border-color: #675CA8;
            color: #675CA8;
        }
        .social ul>li:nth-child(6) a:hover{
            background-color: #675CA8;
            border-color: #675CA8;
            color: #fff;
        }
        .social ul>li:nth-child(7) a{
            padding: 10px 13px;
            border-color: #0088cc;
            color: #0088cc;
        }
        .social ul>li:nth-child(7) a:hover{
            background-color: #0088cc;
            border-color: #0088cc;
            color: #fff;
        }
        .social ul>li:nth-child(8) a{
            padding: 10px 10px;
            border-color: #dd4b39;
            color: #dd4b39;
        }
        .social ul>li:nth-child(8) a:hover{
            background-color: #dd4b39;
            border-color: #dd4b39;
            color: #fff;
        }
        .about-part .about-img{
            background-image: url("../images/about_us.png");
            background-size: cover;
            background-size: 100%;
            background-repeat: no-repeat;
            height: 100%;
        }
        .about-part .about-us{
            padding-top: 30px;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-book-reader"></i> <b>ABOUT </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">About Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< About Part Starts >======== --}}
    <section class="about-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="edit-page">
                        @if (isset($aboutsections->null))
                            <a href="{{ route('about.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Create About"><i class="fas fa-plus"></i></a>
                        @else
                            <a href="{{ route('about.create') }}" class="btn btn-light btn-sm btn-create m-l-10 disabled" data-toggle="tooltip" data-placement="top" title="Create About"><i class="fas fa-plus"></i></a>
                        @endif

                        @foreach ($aboutsections as $aboutsection)                        
                        <a href="{{ route('about.show', $aboutsection -> id) }}" class="btn btn-light btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Update About"><i class="fas fa-pencil-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="title-div text-center p-b-30">
                        <div class="title-head">
                            <h1>ABOUT</h1>
                        </div>
                        <div class="title-caption">
                            <h3><span>who</span> we are..!!</h3>
                        </div>
                    </div>
                </div>
            </div>
              <div class="row">
                <div class="col-md-5 d-none d-sm-block">
                    <div class="about-img">
                        <figure>
                            @if (isset($aboutsection->about_image))
                            <img src="{{ asset('images/'.$aboutsection->about_image) }}" alt="" class="img-responsive" width="100%">
                            @else
                            <img src="{{ asset('files/images/about_us.png') }}" alt="" class="img-responsive" width="100%">
                            @endif
                        </figure>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="about-us">
                        @if (isset($aboutsection->about_heading))
                            <h3>{{$aboutsection->about_heading}}</h3>
                        @else
                            <h3>We are a team</h3>
                        @endif

                        @if (isset($aboutsection->about_details))
                            <p class="text-justify text-muted">{{$aboutsection->about_details}}</p>
                        @else
                            <p class="text-justify text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere quis blanditiis, dignissimos officiis at eos ducimus maiores, maxime delectus laudantium voluptatum molestiae consequatur suscipit perspiciatis accusamus ex rem cumque. <br> Distinctio expedita odio doloremque quia. Libero, unde eos corrupti, iste molestias distinctio tempora dolorum deserunt atque vel dolor veritatis, minus placeat.</p>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="social p-t-40">
                                <ul>
                                    @if (isset($aboutsection->linkdin))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Linkdin"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->skype))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Skype"><i class="fab fa-skype"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->facebook))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->twitter))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->whatsapp))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Whatsapp"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->viber))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Viber"><i class="fab fa-viber"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->telegram))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Telegram"><i class="fab fa-telegram-plane"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                    
                                    @if (isset($aboutsection->google_plus))
                                        <li>
                                            <a href="" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                    @else
                                        <li class="d-none"></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            @endforeach
            
        </div>
    </section>
    {{-- =========< About Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection