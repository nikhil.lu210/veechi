@extends('layouts.main')

@section('page_title', '| About Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
        }
        .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
        }
        .avatar-upload .avatar-edit input {
        display: none;
        }
        .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label i.fas.fa-pencil-alt {
            padding: 9px;
        }
        .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-book-reader"></i> <b>CREATE ABOUT </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item"><a href="{{ route('about') }}">About Section</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< About Part Starts >======== --}}
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-right">
                                <a href="{{ Route('about') }}" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <form action="{{ Route('about.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5" style="border-right: 1px solid #efefef;">
                                        {{--  About Left Part  --}}
                                        <div class="group">

                                            {{--  About Image  --}}
                                            <div class="avatar-upload about">
                                                <div class="avatar-edit">
                                                    <input type='file' name="about_image" id="aboutImageUpload" accept=".png, .jpg, .jpeg" class="{{ $errors->has('about_heading') ? ' is-invalid' : '' }}" />
                                                    <label for="aboutImageUpload"><i class="fas fa-pencil-alt"></i></label>
                                                    @if ($errors->has('about_heading'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('about_heading') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="aboutImagePreview" style="background-image: url(' {{asset('files/images/about_us.png')}} '); background-size: 90%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--  About Right Part  --}}
                                    <div class="col-md-7">
                                        {{--  About Heading  --}}
                                        <div class="form-group">
                                            <label class="control-label">About Heading</label>
                                            <input name="about_heading" class="form-control {{ $errors->has('about_heading') ? ' is-invalid' : '' }}" type="text" placeholder="We are a team">
                                            @if ($errors->has('about_heading'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('about_heading') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  About Us  --}}
                                        <div class="form-group">
                                            <label class="control-label">About Us</label>
                                            <textarea name="about_details" class="form-control {{ $errors->has('about_details') ? ' is-invalid' : '' }}" rows="4" placeholder="Enter your company Details"></textarea>
                                            @if ($errors->has('about_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('about_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Linkdin  --}}
                                        <div class="form-group">
                                            <label class="control-label">Linkdin</label>
                                            <input name="linkdin" class="form-control {{ $errors->has('linkdin') ? ' is-invalid' : '' }}" type="text" placeholder="https://www.linkedin.com/company/veechitechnologies">
                                            @if ($errors->has('linkdin'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('linkdin') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Skype  --}}
                                        <div class="form-group">
                                            <label class="control-label">Skype</label>
                                            <input name="skype" class="form-control {{ $errors->has('skype') ? ' is-invalid' : '' }}" type="text" placeholder="veechi.technologies">
                                            @if ($errors->has('skype'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('skype') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Facebook  --}}
                                        <div class="form-group">
                                            <label class="control-label">Facebook</label>
                                            <input name="facebook" class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}" type="text" placeholder="https://www.facebook.com/VeechiTechnologies">
                                            @if ($errors->has('facebook'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('facebook') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Twitter  --}}
                                        <div class="form-group">
                                            <label class="control-label">Twitter</label>
                                            <input name="twitter" class="form-control {{ $errors->has('twitter') ? ' is-invalid' : '' }}" type="text" placeholder="https://twitter.com/VeechiTechnologies">
                                            @if ($errors->has('twitter'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('twitter') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Whatsapp  --}}
                                        <div class="form-group">
                                            <label class="control-label">Whatsapp</label>
                                            <input name="whatsapp" class="form-control {{ $errors->has('whatsapp') ? ' is-invalid' : '' }}" type="text" placeholder="+880 1712 345678">
                                            @if ($errors->has('whatsapp'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('whatsapp') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Viber  --}}
                                        <div class="form-group">
                                            <label class="control-label">Viber</label>
                                            <input name="viber" class="form-control {{ $errors->has('viber') ? ' is-invalid' : '' }}" type="text" placeholder="+880 1712 345678">
                                            @if ($errors->has('viber'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('viber') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Telegram  --}}
                                        <div class="form-group">
                                            <label class="control-label">Telegram</label>
                                            <input name="telegram" class="form-control {{ $errors->has('telegram') ? ' is-invalid' : '' }}" type="text" placeholder="+880 1712 345678">
                                            @if ($errors->has('telegram'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('telegram') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{--  Google Plus  --}}
                                        <div class="form-group">
                                            <label class="control-label">Google Plus</label>
                                            <input name="google_plus" class="form-control {{ $errors->has('google_plus') ? ' is-invalid' : '' }}" type="text" placeholder="https://plus.google.com/u/0/+VEECHITECHNOLOGIES">
                                            @if ($errors->has('google_plus'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('google_plus') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{--  Submit Button  --}}
                                <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                    <button type="submit" class="btn btn-light custom-btn"><i class="far fa-thumbs-up"></i> Add Now</button>
                                </div>
                            </div>

                        </form> {{--  Form Ends  --}}
                    </div>
                </div>
            </div>
        </div>
    {{-- =========< About Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        //For Changing About Image
        $(".avatar-upload.about").ready(function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#aboutImagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#aboutImagePreview').hide();
                        $('#aboutImagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            $("#aboutImageUpload").change(function() {
                readURL(this);
            });
        });
    </script>
@endsection