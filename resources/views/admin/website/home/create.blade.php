@extends('layouts.main')

@section('page_title', '| Home Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
        }
        .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
        }
        .avatar-upload .avatar-edit input {
        display: none;
        }
        .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label i.fas.fa-pencil-alt {
            padding: 9px;
        }
        .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-home"></i> <b>CREATE HOME</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Home Section</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Homepage Part Starts >======== --}}
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        {{--  Back Button  --}}
                        <div class="float-right">
                            <a href="{{ route('homepage') }}" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                        </div>
                    </div>

                    {{-- Form Starts Here --}}
                    <form action="{{ Route('homepage.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7" style="border-right: 1px solid #efefef;">

                                    {{--  Logo Image  --}}
                                    <div class="group">
                                        <div class="avatar-upload homeLogo">
                                            <div class="avatar-edit">
                                                <input type='file' name="company_logo" id="logoUpload" accept=".png, .jpg, .jpeg" />
                                                <label for="logoUpload"><i class="fas fa-pencil-alt"></i></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="logoPreview" style="background-image: url('{{asset('files/images/logo02.png')}}'); background-size: 90%;"></div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--  Company Moto  --}}
                                    <div class="form-group">
                                        <label class="control-label">Company Moto</label>
                                        <textarea class="form-control" rows="2" placeholder="We solve UX problems to achive your business goal...!" name="company_moto"></textarea>
                                    </div>

                                    {{--  Company Name  --}}
                                    <div class="form-group">
                                        <label class="control-label">Company Name</label>
                                        <input class="form-control" type="text" placeholder="Veechi Technologies" name="company_name">
                                    </div>

                                    {{--  Short Note  --}}
                                    <div class="form-group">
                                        <label class="control-label">Short Note</label>
                                        <textarea class="form-control" rows="4" placeholder="Enter your company short notes" name="short_note"></textarea>
                                    </div>
                                </div>

                                {{--  Home Right Image  --}}
                                <div class="group">
                                    <div class="avatar-upload homeBackground" style="margin: 56px 100px;">
                                        <div class="avatar-edit">
                                            <input type='file' name="background_image" id="homeBackgroundUpload" accept=".png, .jpg, .jpeg" />
                                            <label for="homeBackgroundUpload"><i class="fas fa-pencil-alt"></i></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="homeBackgroundPreview" style="background-image: url(' {{asset('files/images/header_image1.png')}} '); background-size: 90%;"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                        <div class="card-footer">
                            <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                {{--  Submit Button  --}}
                                <button type="submit" class="btn btn-light custom-btn"><i class="far fa-thumbs-up"></i> Add Now</button>
                            </div>
                        </div>

                    </form> 
                    {{-- Form End Here --}}

                </div>
            </div>
        </div>
    </div>
    {{-- =========< Homepage Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        //For Changing LOGO
        $(".avatar-upload.homeLogo").ready(function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#logoPreview').css('background-image', 'url('+e.target.result +')');
                    $('#logoPreview').hide();
                    $('#logoPreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $("#logoUpload").change(function() {
                readURL(this);
            });
        });

        //For Changing Homepage Image
        $(".avatar-upload.homeBackground").ready(function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#homeBackgroundPreview').css('background-image', 'url('+e.target.result +')');
                    $('#homeBackgroundPreview').hide();
                    $('#homeBackgroundPreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $("#homeBackgroundUpload").change(function() {
                readURL(this);
            });
        });
    </script>
@endsection