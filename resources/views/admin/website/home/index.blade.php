@extends('layouts.main')

@section('page_title', '| HOME')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .home-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
            }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-home"></i> <b>HOME </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">Home Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Home Part Starts >======== --}}
    <section class="home-part">
        <div class="container">
            
            <div class="row">
                <div class="col-12">
                    @foreach ($homesections as $homesection)
                    <div class="edit-page">
                        @if (isset($homesection))
                            <a href="#" class="btn btn-light btn-sm btn-create m-l-10 disabled" data-toggle="tooltip" data-placement="top" title="Create Home"><i class="fas fa-plus"></i></a>
                        @else
                            <a href="{{ route('homepage.create') }}" class="btn btn-light btn-sm btn-create m-l-10" data-toggle="tooltip" data-placement="top" title="Create Home"><i class="fas fa-plus"></i></a>
                        @endif

                        @if (isset($homesection))
                            <a href="{{ route('homepage.show', $homesection->id) }}" class="btn btn-light btn-sm btn-edit" data-toggle="tooltip" data-placement="top" title="Update Home"><i class="fas fa-pencil-alt"></i></a>
                        @else
                            <a href="#" class="btn btn-light btn-sm btn-edit disabled" data-toggle="tooltip" data-placement="top" title="Update Home"><i class="fas fa-pencil-alt"></i></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    {{--  Company Logo  --}}
                    <div class="veechi-logo text-center m-t-10">
                        <figure><img src="{{ asset('images/'.$homesection->company_logo) }}" alt="L O G O" width="20%"></figure>
                    </div>

                    <div class="page-caption">
                        {{--  Company Moto  --}}
                        <h6 class="veechi-moto text-center">
                            {{ $homesection->company_moto }}
                        </h6>

                        {{--  Company Name  --}}
                        <h1 class="veechi-welcome m-t-10 text-center">
                            welcome to <br>
                            <span class="veechi">{{ $homesection->company_name }}</span>
                        </h1>

                        {{--  Short Note  --}}
                        <p class="veechi-info text-center">
                            {{ $homesection->short_note }}
                        </p>
                        <div class="row p-t-50">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-6">
                                        <a href="" class="btn btn-success btn-lg btn-block btn-custom">Get Started</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="" class="btn btn-outline-success btn-lg btn-block btn-outline-custom">Our Services</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="home-image">

                        {{--  Home Right Image  --}}
                        <figure><img src="{{ asset('images/'.$homesection->background_image) }}" alt="L O G O" width="100%"></figure>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
    {{-- =========< Home Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection