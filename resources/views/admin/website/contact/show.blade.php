@extends('layouts.main')

@section('page_title', '| Read Message')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        h5{
            font-weight: 500;
        }
        h5 > span{
            font-weight: 400;
            color: #666666;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-comments"></i> <b>READ MESSAGE </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item"><a href="{{ route('contact') }}">Contact Section</a></li>
                <li class="breadcrumb-item active">View Message</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Contact Part Starts >======== --}}
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                        </div>
                    </div>
                    <form action="" method="">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 p-b-20">
                                    <h5>SUBJECT: <span class="subject"><br> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span></h5>
                                </div>
                                <div class="col-md-6 p-b-20">
                                    <h5>NAME: <span class="name"><br> Tanjil Ahmed Fahim</span></h5>
                                </div>
                                <div class="col-md-6 p-b-20">
                                    <h5>EMAIL: <span class="email"><br> fahim@veechi.com</span></h5>
                                </div>
                                <div class="col-md-12 p-b-20">
                                    <h5>MESSAGE: <span class="message"><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe veritatis laborum aut aspernatur, ut incidunt nihil sit ipsa ipsum voluptas repellat quam architecto sequi molestias nostrum ullam facere consequatur. Culpa possimus iste veniam fugit dicta necessitatibus, perferendis libero mollitia laborum molestias, magni saepe, accusamus nulla sed reiciendis aliquid quam exercitationem!</span></h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-light custom-btn" id="replyBtn"><i class="fas fa-reply"></i> Reply</button>

                                <button type="button" class="btn btn-light custom-btn hiddenButton d-none" id="cancleBtn"><i class="far fa-thumbs-down"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container reply-message m-t-30" style="display: none;">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="" method="">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Subject</label>
                                                <input class="form-control" type="text" placeholder="Veechi Technologies">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Name</label>
                                                <input class="form-control" type="text" placeholder="Veechi Technologies">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Reply Message</label>
                                                <textarea class="form-control" rows="4" placeholder="Enter your reply message..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                <button type="submit" class="btn btn-light custom-btn"><i class="fas fa-reply"></i> Reply Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- =========< Contact Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function(){
            $("#replyBtn").click(function(){
                $('#cancleBtn').removeClass('d-none');
                $('#replyBtn').addClass('d-none');
            });
            $("#cancleBtn").click(function(){
                $('#replyBtn').removeClass('d-none');
                $('#cancleBtn').addClass('d-none');       
            });
            $("#replyBtn, #cancleBtn").click(function(){
                $(".reply-message").slideToggle();
                return false
            });
        });
    </script>
@endsection