@extends('layouts.main')

@section('page_title', '| CONTACT')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .contact-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
            }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }

        .table th, .table td, .table thead th{
            border-color: #fff;
            text-align: center;
        }
        .table thead th{
            border-bottom: 1px solid #fbfbfb;
        }
        .table thead tr th{
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
        }
        .table tbody tr th{
            font-weight: 500;
        }
        .table-hover tbody tr{
            transition: 0.3s all ease-in-out;
        }
        .table-hover tbody tr:hover {
            background-color: rgba(202, 200, 200, 0.075);
            transition: 0.3s all ease-in-out;
        }
        .table th, .table td {
            padding: 0.95rem 0.75rem 0.75rem;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-comments"></i> <b>CONTACT </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Website Sections</li>
                <li class="breadcrumb-item active">Contact Section</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Contact Part Starts >======== --}}
    <section class="contact-part">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-borderless table-hover">
                        <thead>
                          <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">12-12-2018</th>
                            <th scope="row">12:00 AM</th>
                            <th scope="row">Tanjil Ahmed Fahim</th>
                            <th scope="row">tanjil@fahim.com</th>
                            <td style="padding-top: 10px;">
                                <a href="#" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete Message"><i class="far fa-trash-alt"></i></a>
                                <a href="{{ route('contact.show', 1) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Message"><i class="far fa-question-circle"></i></a>
                            </td>
                          </tr>
                          <tr>
                            <th scope="row">12-12-2018</th>
                            <th scope="row">12:00 AM</th>
                            <th scope="row">Sourav Roy Avijeet</th>
                            <th scope="row">sourav.lu246@gmail.com</th>
                            <td style="padding-top: 10px;">
                                <a href="#" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete Message"><i class="far fa-trash-alt"></i></a>
                                <a href="{{ route('contact.show', 1) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Message"><i class="far fa-question-circle"></i></a>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Home Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection