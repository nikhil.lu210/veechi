@extends('layouts.main')

@section('page_title', '| Career | Create')

@section('stylesheet')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('admin-files/css/nice-select/nice-select.css') }}">

    <style>
        .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
        margin-top: 10px;
        }
        .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
        }
        .avatar-upload .avatar-edit input {
        display: none;
        }
        .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label i.fas.fa-pencil-alt {
            padding: 9px;
        }
        .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        }

        .card.border-light {
            border-color: #ed1b24!important;
        }
        .card-header.border-light, .card-footer.border-light{
            background-color: #ed1b24;
        }
        .card-header.border-light h4{
            margin-bottom: 0px;
            text-transform: uppercase;
            font-weight: bold;
            color: #ffffff;
        }
        .card-header.border-light h5{
            margin-bottom: 0px;
            text-transform: uppercase;
            font-weight: bold;
            color: #ffffff;
            padding-top: 3px;
        }

        label{
            font-weight: 500;
        }
        .form-control {
            height: calc(2.25rem + 2px);
            padding: 7px 10px;
            font-weight: 500;
            color: #222222;
            border: 1px solid #dddddd;
            border-radius: .25rem;
            box-shadow: none !important;
            transition: 0.5s all ease-in-out;
        }
        .form-control:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small {
            font-weight: 500;
            font-size: 14px;
            height: calc(2.25rem + 2px);
            line-height: 38px;
            border-color: #dddddd;
            transition: 0.5s all ease-in-out;
        }
        .nice-select:active, 
        .nice-select.open, 
        .nice-select:focus{
            border-color: #ed1b24;
            transition: 0.5s all ease-in-out;
        }
        .nice-select.small .option {
            font-weight: 500;
        }

        .btn-default{
            background: #ffffff;
            font-weight: bold;
            color: #333333;
            outline: none !important;
            padding: 3px 6px;
            transition: 0.3s all ease-in-out;
        }
        .btn-default:hover,
        .btn-default:focus,
        .btn-default:active{
            color: #000000;
            outline: none !important;
            box-shadow: none !important;
            transform: none !important;
            transition: 0.3s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-parachute-box"></i> <b>NEW CAREER POST</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Extra Pages</li>
                <li class="breadcrumb-item"><a href="{{ route('career') }}">Career</a></li>
                <li class="breadcrumb-item active">New Career Post</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< career Part Starts >======== --}}
        <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="float-right">
                                    <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                                </div>
                            </div>
                            <form action="" method="">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4 offset-md-4">
                                            {{--  career thumbnail Part  --}}
                                            <div class="group">
                                                <div class="avatar-upload career">
                                                    <div class="avatar-edit">
                                                        <input type='file' name="avatar" id="careerImageUpload" accept=".png, .jpg, .jpeg" />
                                                        <label for="careerImageUpload"><i class="fas fa-pencil-alt"></i></label>
                                                    </div>
                                                    <div class="avatar-preview">
                                                        <div id="careerImagePreview" style="background-image: url(' {{asset('files/images/about_us.png')}} '); background-size: 90%;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--  career post Part  --}}
                                        <div class="col-md-12">
                                            <div class="card border-light">
                                                <div class="card-header border-light">
                                                    <h4>JOB SUMMARY</h4>
                                                </div>
                                                <div class="card-body border-light">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Title</label>
                                                                <input class="form-control" type="text" placeholder="UI/UX Designer Required">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Status</label><br>
                                                                <select class="small wide">
                                                                    <option data-display="Select Status">Select Status</option>
                                                                    <option value="1">Fulltime</option>
                                                                    <option value="2">Parttime</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Gender</label><br>
                                                                <select class="small wide">
                                                                    <option data-display="Select Gender">Select Gender</option>
                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>
                                                                    <option value="3">Male/Female</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Age From</label>
                                                                <input class="form-control" type="text" placeholder="20">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Age To</label>
                                                                <input class="form-control" type="text" placeholder="30">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Experience</label>
                                                                <input class="form-control" type="text" placeholder="02">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Salary</label>
                                                                <input class="form-control" type="text" placeholder="20000">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Job Location</label>
                                                                <input class="form-control" type="text" placeholder="Sylhet">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Application Deadline</label>
                                                                <input class="form-control" type="date">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-30">
                                        <div class="col-md-6">
                                            <div class="card border-light">
                                                <div class="card-header border-light">
                                                    <h5 class="float-left">Job Responsibilities</h5>
                                                    <a href="#" class="btn btn-default btn-sm float-right" id="addJobRes"><i class="fas fa-plus-circle"></i></a>
                                                </div>
                                                <div class="card-body border-light">
                                                    <div class="row">
                                                        <div class="col-md-12" id="jobResponsibility">
                                                            <div class="form-group" id="jobRes">
                                                                <textarea class="form-control" name="jobRes" rows="2" placeholder="Job Responsibilities"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card border-light exp-requirements">
                                                <div class="card-header border-light">
                                                    <h5 class="float-left">Experience Requirements</h5>
                                                    <a href="#" class="btn btn-default btn-sm float-right" id="addJobExp"><i class="fas fa-plus-circle"></i></a>
                                                </div>
                                                <div class="card-body border-light">
                                                    <div class="row">
                                                        <div class="col-md-12" id="jobExperience">
                                                            <div class="form-group">
                                                                <textarea class="form-control" name="jobExp" id="jobExp" rows="2" placeholder="Experience Requirements"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-30">
                                        <div class="col-md-6">
                                            <div class="card border-light edu-requirements">
                                                <div class="card-header border-light">
                                                    <h5 class="float-left">Educational Requirements</h5>
                                                    <a href="#" class="btn btn-default btn-sm float-right" id="addJobEdu"><i class="fas fa-plus-circle"></i></a>
                                                </div>
                                                <div class="card-body border-light">
                                                    <div class="row">
                                                        <div class="col-md-12" id="jobEducation">
                                                            <div class="form-group">
                                                                <textarea class="form-control" name="jobEdu" id="jobEdu" rows="2" placeholder="Job Education"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card border-light job-benefits">
                                                <div class="card-header border-light">
                                                    <h5 class="float-left">Compensation & Other Benefits</h5>
                                                    <a href="#" class="btn btn-default btn-sm float-right" id="addJobBen"><i class="fas fa-plus-circle"></i></a>
                                                </div>
                                                <div class="card-body border-light">
                                                    <div class="row">
                                                        <div class="col-md-12" id="jobBenefit">
                                                            <div class="form-group">
                                                                <textarea class="form-control" name="jobBen" id="jobBen" rows="2" placeholder="Compensation & Other Benefits"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                        <button type="submit" class="btn btn-light custom-btn"><i class="far fa-thumbs-up"></i> Add New Job</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        {{-- =========< career Part Ends >========= --}}
        
    @endsection
    
    @section('scripts')
        {{--  External Javascript  --}}
        <script src="{{ asset('admin-files/js/nice-select/jquery.nice-select.min.js') }}"></script>
        <script>
            //For Changing career Image
            $(".avatar-upload.career").ready(function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#careerImagePreview').css('background-image', 'url('+e.target.result +')');
                            $('#careerImagePreview').hide();
                            $('#careerImagePreview').fadeIn(650);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                $("#careerImageUpload").change(function() {
                    readURL(this);
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('select').niceSelect();
            });
        </script>
        <script>
            $(document).ready(function() {
                var jobResDiv = $('#jobResponsibility');
                $('#addJobRes').click(function() {
                    $('<div class="form-group" id="jobRes"> <textarea class="form-control" name="jobRes" rows="2" placeholder="Job Responsibilities"></textarea> </div>').appendTo(jobResDiv);
                    return false;
                });

                var jobExpDiv = $('#jobExperience');
                $('#addJobExp').click(function() {
                    $('<div class="form-group" id="jobRes"> <textarea class="form-control" name="jobRes" rows="2" placeholder="Job Experience"></textarea> </div>').appendTo(jobExpDiv);
                    return false;
                });

                var jobEduDiv = $('#jobEducation');
                $('#addJobEdu').click(function() {
                    $('<div class="form-group" id="jobRes"> <textarea class="form-control" name="jobRes" rows="2" placeholder="Job Education"></textarea> </div>').appendTo(jobEduDiv);
                    return false;
                });

                var jobBenDiv = $('#jobBenefit');
                $('#addJobBen').click(function() {
                    $('<div class="form-group" id="jobRes"> <textarea class="form-control" name="jobRes" rows="2" placeholder="Compensation & Other Benefits"></textarea> </div>').appendTo(jobBenDiv);
                    return false;
                });
            });
        </script>
    @endsection