@extends('layouts.main')

@section('page_title', '| CAREER | Application')

@section('stylesheet')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('admin-files/css/nice-select/nice-select.css') }}">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');

        .career-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
        }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }
        th{
            text-transform: uppercase;
        }
        .table th, .table td, .table thead th{
            border-color: #fff;
            text-align: center;
        }
        .table-hover tbody tr{
            transition: 0.3s all ease-in-out;
        }
        .table-hover tbody tr:hover {
            background-color: rgba(202, 200, 200, 0.075);
            transition: 0.3s all ease-in-out;
        }
        .table th, .table td {
            padding: 0.95rem 0.75rem 0.75rem;
        }
        .table th{
            font-weight: 500;
        }
        .table td {
            font-weight: 400;
            color: #333333;
        }

        .social ul{
            margin-bottom: 5px;
        }
        .social ul>li{
            list-style: none;
            display: inline;
            margin-right: 5px;
            margin-left: 0px;
        }
        .social ul>li a{
            border: 1px solid;
            border-radius: 3px;
            transition: 0.3s all ease-in-out;
        }
        .social ul>li a:hover{
            border: 1px solid;
            transition: 0.3s all ease-in-out;
        }
        .social ul>li:nth-child(1) a{
            padding: 5px 8px;
            border-color: #0077B5;
            color: #0077B5;
        }
        .social ul>li:nth-child(1) a:hover{
            background-color: #0077B5;
            border-color: #0077B5;
            color: #fff;
        }
        .social ul>li:nth-child(2) a{
            padding: 5px 10px;
            border-color: #3b5999;
            color: #3b5999;
        }
        .social ul>li:nth-child(2) a:hover{
            background-color: #3b5999;
            border-color: #3b5999;
            color: #fff;
        }
        .social ul>li:nth-child(3) a{
            padding: 5px 8px;
            border-color: #333;
            color: #333;
        }
        .social ul>li:nth-child(3) a:hover{
            background-color: #333;
            border-color: #333;
            color: #fff;
        }
        .social ul>li:nth-child(4) a{
            padding: 5px 6px;
            border-color: #dd4b39;
            color: #dd4b39;
        }
        .social ul>li:nth-child(4) a:hover{
            background-color: #dd4b39;
            border-color: #dd4b39;
            color: #fff;
        }
        a.cv-resume{
            color: #ed1b24;
            border: 1px solid #ed1b24;
            padding: 7px 10px;
            border-radius: 3px;
            transition: 0.3s all ease-in-out;
        }
        a.cv-resume:hover{
            color: #ffffff;
            background-color: #ed1b24;
            text-decoration: none;
            transition: 0.3s all ease-in-out;
        }
        .nice-select.small {
            font-size: 14px;
            height: 38px;
            line-height: 35px;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-parachute-box"></i> <b>APPLICATION </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Extra Pages</li>
                <li class="breadcrumb-item"><a href="{{ route('career') }}">Career</a></li>
                <li class="breadcrumb-item"><a href="{{ route('career.show', 1) }}">Applicants</a></li>
                <li class="breadcrumb-item active">Applicantion</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Career Part Starts >======== --}}
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                        </div>
                    </div>
                    <form action="" method="">
                        <div class="card-body">
                            <table class="table table-borderless table-hover">
                                <tbody>
                                    <tr>
                                        <th style="text-align: left !important; width:40%;">Full Name:</th>
                                        <td style="text-align: left !important;">Md. Tanjil Ahmed Fahim</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width:40%;">Email Address:</th>
                                        <td style="text-align: left !important;">ahmedtanjil@gmail.com</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width430%;">Mobile Number:</th>
                                        <td style="text-align: left !important;">+8801712345678</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width430%;">Social Links:</th>
                                        <td style="text-align: left !important;">
                                            <div class="social">
                                                <ul>
                                                    <li>
                                                        <a href="" data-toggle="tooltip" data-placement="top" title="Linkdin" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="" data-toggle="tooltip" data-placement="top" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="" data-toggle="tooltip" data-placement="top" title="Github" target="_blank"><i class="fab fa-github"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="" data-toggle="tooltip" data-placement="top" title="Google Plus" target="_blank"><i class="fab fa-google-plus-g"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width430%;">Experience <sup>(year)</sup> :</th>
                                        <td style="text-align: left !important;"> 03</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width430%;">Location :</th>
                                        <td style="text-align: left !important;"> Optimum Tower, 2nd Floor, Lamabazar, Sylhet</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width:40%;">About :</th>
                                        <td style="text-align: justify !important;">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Obcaecati maiores, praesentium consectetur quasi omnis fuga enim voluptatibus dolorem dolores delectus eaque commodi aperiam, id ducimus accusamus rem.</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left !important; width:40%;">CV / RESUME :</th>
                                        <td style="text-align: left !important;">
                                            <a href="#" class="cv-resume" target="_blank" data-toggle="tooltip" data-placement="top" title="Download CV/Resume"><i class="fas fa-cloud-download-alt"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                <a href="" class="btn btn-light custom-btn" id="rejectBtn"><i class="fas fa-ban"></i> Reject</a>

                                <button type="button" class="btn btn-light custom-btn" id="acceptBtn"><i class="far fa-thumbs-up"></i> Accept</button>

                                <button type="button" class="btn btn-light custom-btn hiddenButton d-none" id="cancleBtn"><i class="far fa-times-circle"></i> Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container send-message m-t-30" style="display: none;">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <form action="" method="">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Select Category</label><br>
                                                <select class="small wide">
                                                    <option data-display="Select Category">Select Category</option>
                                                    <option value="1">First Category</option>
                                                    <option value="2">Second Category</option>
                                                    <option value="3">Thired Category</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Interview Date</label>
                                                <input class="form-control" type="date">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Interview Time</label>
                                                <input class="form-control" type="time">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right m-b-10" role="group" aria-label="Basic example">
                                <button type="submit" class="btn btn-light custom-btn"><i class="fab fa-telegram-plane"></i> Send Interview Invitation</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- =========< Contact Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('admin-files/js/nice-select/jquery.nice-select.min.js') }}"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(function(){
            $("#acceptBtn").click(function(){
                $('#cancleBtn').removeClass('d-none');
                $('#acceptBtn').addClass('d-none');
                $('#rejectBtn').addClass('d-none');
            });
            $("#cancleBtn").click(function(){
                $('#acceptBtn').removeClass('d-none');
                $('#cancleBtn').addClass('d-none'); 
                $('#rejectBtn').removeClass('d-none');      
            });
            $("#acceptBtn, #cancleBtn").click(function(){
                $(".send-message").slideToggle();
                return false
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('select').niceSelect();
        });
    </script>
@endsection