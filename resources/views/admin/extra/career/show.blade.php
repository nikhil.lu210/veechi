@extends('layouts.main')

@section('page_title', '| CAREER')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        @import url('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700');
    
    
        .career-part{
            font-family: 'Quicksand', sans-serif;
            background: #fff;
            padding: 20px 7px 20px 20px;
            border-radius: 5px;
        }
        .btn{
            transform: none !important;
        }
        .tooltip-inner {
            padding: 7px 15px;
            border-radius: 1rem;
        }

        .table th, .table td, .table thead th{
            border-color: #fff;
            text-align: center;
        }
        .table thead th{
            border-bottom: 1px solid #fbfbfb;
        }
        .table thead tr th{
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
        }
        .table tbody tr th{
            font-weight: 500;
        }
        .table-hover tbody tr{
            transition: 0.3s all ease-in-out;
        }
        .table-hover tbody tr:hover {
            background-color: rgba(202, 200, 200, 0.075);
            transition: 0.3s all ease-in-out;
        }
        .table th, .table td {
            padding: 0.95rem 0.75rem 0.75rem;
        }
        a.btn.btn-light.btn-sm.btn-edit-job {
            border: 1px solid #00000050;
            border-radius: 30px;
            padding: 5px 8px;
            color: #000000;
            background-color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
        a.btn.btn-light.btn-sm.btn-edit-job:focus,
        a.btn.btn-light.btn-sm.btn-edit-job:active,
        a.btn.btn-light.btn-sm.btn-edit-job:hover{
            box-shadow: none;
            background-color: #ed1b24;
            border-color: #ed1b24;
            color: #ffffff;
            transition: 0.3s all ease-in-out;
        }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-parachute-box"></i> <b>CAREER </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Extra Pages</li>
                <li class="breadcrumb-item"><a href="{{ route('career') }}">Career</a></li>
                <li class="breadcrumb-item active">Applicants</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Career Part Starts >======== --}}
    <section class="career-part">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="float-left">
                        <div class="app-search">
                            <input class="app-search__input" type="search" placeholder="Search" id="searchTable">
                        </div>
                    </div>
                    <div class="float-right">
                        <a onclick="history.back()" class="btn btn-light btn-back"><i class="fas fa-hand-point-left"></i> Back</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-borderless table-hover">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Applicant Name</th>
                            <th scope="col">Experience Year</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">01</th>
                            <th scope="row">01-01-2019</th>
                            <th scope="row">08:23 AM</th>
                            <th scope="row">Tanjil Ahmed Fahim</th>
                            <th scope="row">05</th>
                            <td style="padding-top: 10px;">

                                <a href="#" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete Application"><i class="far fa-trash-alt"></i></a>

                                <a href="{{ route('career.edit', 1) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Applicantion"><i class="far fa-question-circle"></i></a>

                            </td>
                          </tr>
                          <tr>
                            <th scope="row">02</th>
                            <th scope="row">02-01-2019</th>
                            <th scope="row">06:00 AM</th>
                            <th scope="row">Sourav Roy Aveejit</th>
                            <th scope="row">50</th>
                            <td style="padding-top: 10px;">

                                <a href="#" class="btn btn-light btn-sm btn-delete" data-toggle="tooltip" data-placement="top" title="Delete Application"><i class="far fa-trash-alt"></i></a>

                                <a href="{{ route('career.edit', 1) }}" class="btn btn-light btn-sm btn-view" data-toggle="tooltip" data-placement="top" title="View Applicantion"><i class="far fa-question-circle"></i></a>

                            </td>
                          </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Home Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function(){
            $("#searchTable").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection