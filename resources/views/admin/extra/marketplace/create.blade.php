@extends('layouts.main')

@section('page_title', '| Extra Pages | Career')

@section('stylesheet')
    {{--  External CSS  --}}
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-book-reader"></i> <b>CAREER </b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Extra Pages</li>
                <li class="breadcrumb-item active">Career</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Part Starts >======== --}}
    
    {{-- =========< Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
@endsection