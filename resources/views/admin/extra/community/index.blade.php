@extends('layouts.main')

@section('page_title', '| COMMUNICATION')

@section('stylesheet')
    {{--  External CSS  --}}
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fab fa-connectdevelop"></i> <b>COMMUNITY</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Extra Pages</li>
                <li class="breadcrumb-item active">Community</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Part Starts >======== --}}
    
    {{-- =========< Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
@endsection