@extends('layouts.main')

@section('page_title', '| Dashboard')

@section('stylesheet')
    {{--  External CSS  --}}
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    
    {{-- ========< Breadcrumb Part Starts >======== --}}
    <section class="breadcrumb-part">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-dice-d6"></i> <b>DASHBOARD</b></h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home fa-lg"></i></a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ul>
        </div>
    </section>
    {{-- =========< Breadcrumb Part Ends >========= --}}


    {{-- ========< Dashboard Part Starts >======== --}}
    <section class="dashboard-part">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="widget-small primary coloured-icon"><i class="icon fas fa-users fa-3x"></i>
                        <div class="info">
                            <h4>Members</h4>
                            <p><b>19</b></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="widget-small info coloured-icon"><i class="icon fas fa-thumbs-up fa-3x"></i>
                        <div class="info">
                            <h4>Likes</h4>
                            <p><b>25</b></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="widget-small warning coloured-icon"><i class="icon fas fa-cloud-upload-alt fa-3x"></i>
                        <div class="info">
                            <h4>Uploades</h4>
                            <p><b>10</b></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="widget-small danger coloured-icon"><i class="icon fas fa-star fa-3x"></i>
                        <div class="info">
                            <h4>Stars</h4>
                            <p><b>500</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- =========< Dashboard Part Ends >========= --}}
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
@endsection